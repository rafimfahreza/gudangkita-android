package prokoding.gudangkita.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.utils.TextUtils;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder> {
    public OnClickCallback onClickCallback = new OnClickCallback() {
        public void onClicked(Inventory.Data data) {
        }
    };
    Context context;
    private ArrayList<Inventory.Data> data = new ArrayList<>();

    public InventoryAdapter(ArrayList<Inventory.Data> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_inventory, viewGroup, false);
        this.context = viewGroup.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.title.setText(data.get(position).productName);
        viewHolder.detail.setText(TextUtils.simplifyAmount(data.get(position).baseStock) + "\nin stock");
        Glide.with(context)
                .load(data.get(position).productImage)
                .centerCrop()
                .into(viewHolder.image);

        viewHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCallback.onClicked(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public interface OnClickCallback {
        void onClicked(Inventory.Data data);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final CardView card;
        public final ImageView image;
        public final TextView title;
        public final TextView detail;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.item_image);
            title = view.findViewById(R.id.item_title);
            detail = view.findViewById(R.id.item_detail);
            card = view.findViewById(R.id.item_card);
        }
    }
}

