package prokoding.gudangkita.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import prokoding.gudangkita.R;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    public static final int STATE_DANGER = 2;
    public static final int STATE_NEUTRAL = -1;
    public static final int STATE_WARNING = 1;
    public static final int STATE_SUCCESS = 0;
    private final ArrayList<String> titles = new ArrayList<>();
    private final ArrayList<String> details = new ArrayList<>();
    private final ArrayList<Integer> status = new ArrayList<>();
    public Callback callback = null;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_notification, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.title.setText(titles.get(position));
        if (details.get(position) != null) {
            viewHolder.detail.setText(details.get(position));
            viewHolder.detail.setVisibility(View.VISIBLE);
        } else {
            viewHolder.detail.setVisibility(View.GONE);
        }
        int status = this.status.get(position);
        if (status == STATE_DANGER) {
            viewHolder.bar_warning.setVisibility(View.GONE);
            viewHolder.bar_safe.setVisibility(View.GONE);
            viewHolder.bar_danger.setVisibility(View.VISIBLE);
        } else if (status == STATE_WARNING) {
            viewHolder.bar_safe.setVisibility(View.GONE);
            viewHolder.bar_danger.setVisibility(View.GONE);
            viewHolder.bar_warning.setVisibility(View.VISIBLE);
        } else if (status == STATE_SUCCESS) {
            viewHolder.bar_danger.setVisibility(View.GONE);
            viewHolder.bar_warning.setVisibility(View.GONE);
            viewHolder.bar_safe.setVisibility(View.VISIBLE);
        } else {
            viewHolder.bar_danger.setVisibility(View.GONE);
            viewHolder.bar_warning.setVisibility(View.GONE);
            viewHolder.bar_safe.setVisibility(View.GONE);
        }
        if (callback != null) {
            viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onClick(titles.get(position), details.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.titles.size();
    }

    public void addItem(String title, String detail, int state) {
        titles.add(title);
        details.add(detail);
        status.add(state);
        notifyItemChanged(getItemCount());
    }

    public interface Callback {
        void onClick(String title, String detail);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View bar_safe;
        public final View bar_warning;
        public final View bar_danger;
        public final TextView title;
        public final TextView detail;
        public final CardView cardView;

        public ViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.item_card);
            bar_safe = view.findViewById(R.id.item_safe);
            bar_warning = view.findViewById(R.id.item_warning);
            bar_danger = view.findViewById(R.id.item_danger);
            title = view.findViewById(R.id.item_title);
            detail = view.findViewById(R.id.item_detail);
        }
    }
}

