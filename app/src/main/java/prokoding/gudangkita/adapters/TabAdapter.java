package prokoding.gudangkita.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.color.MaterialColors;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.utils.TextUtils;

public class TabAdapter extends RecyclerView.Adapter<TabAdapter.ViewHolder> {
    private int activeId = 0;

    public OnClickCallback onClickCallback = new OnClickCallback() {
        public void onClicked(View v, int position) {
        }
    };
    Context context;
    private ArrayList<String> labels = new ArrayList<>();

    public TabAdapter(ArrayList<String> labels, int defaultSelected) {
        this.labels = labels;
        this.activeId = defaultSelected;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_button, viewGroup, false);
        this.context = viewGroup.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.button.setText(labels.get(position));
        if (position == activeId) {
            viewHolder.button.setBackgroundColor(MaterialColors.getColor(context, R.attr.colorPrimary, context.getResources().getColor(R.color.red)));
            viewHolder.button.setTextColor(MaterialColors.getColor(context, R.attr.colorOnPrimary, Color.WHITE));
        } else {
            viewHolder.button.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.button.setTextAppearance(context, R.style.Theme_GudangKita_TextView_Body);
            viewHolder.button.setTextColor(MaterialColors.getColor(context, R.attr.colorPrimary, context.getResources().getColor(R.color.red)));
        }
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int oldPosition = activeId;
                activeId = position;
                notifyItemChanged(oldPosition);
                notifyItemChanged(position);
                onClickCallback.onClicked(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.labels.size();
    }

    public interface OnClickCallback {
        void onClicked(View v, int position);
    }

    public void setActiveId(int position) {
        int oldPosition = activeId;
        activeId = position;
        notifyItemChanged(oldPosition);
        notifyItemChanged(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final MaterialButton button;

        public ViewHolder(View view) {
            super(view);
            button = view.findViewById(R.id.button);
        }
    }
}

