package prokoding.gudangkita.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import prokoding.gudangkita.R;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    public Callback callback = null;
    private ArrayList<Data> data = new ArrayList<>();

    public HistoryAdapter() {
        super();
    }

    public HistoryAdapter(ArrayList<Data> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_pending, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.title.setText(data.get(position).title);
        viewHolder.detail.setText(data.get(position).detail);
        if (callback != null) {
            viewHolder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onMoreClicked(data.get(position), position);
                }
            });
        } else {
            viewHolder.more.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public void addData(Data data) {
        this.data.add(data);
        notifyItemInserted(getItemCount());
    }

    public void clearData() {
        notifyItemRangeRemoved(0, getItemCount());
        this.data.clear();
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onMoreClicked(Data data, int position);
    }

    public static class Data {
        public String title;
        public String detail;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView detail;
        public final FloatingActionButton more;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.item_title);
            detail = view.findViewById(R.id.item_detail);
            more = view.findViewById(R.id.item_more);
        }
    }
}

