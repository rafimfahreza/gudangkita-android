package prokoding.gudangkita.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.StockoutHistoryDetail;

public class StockoutPictureAdapter extends RecyclerView.Adapter <StockoutPictureAdapter.ViewHolder> {

    Context context;
    private ArrayList<StockoutHistoryDetail.Data.Product> data = new ArrayList<>();

    public StockoutPictureAdapter (ArrayList<StockoutHistoryDetail.Data.Product> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stockout_picture, parent, false);
        this.context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = this.data.get(position).productName;
        String picUrl = this.data.get(position).packagePicture;
        holder.productNameLabel.setText(name);
        Glide.with(context)
                .load(picUrl)
                .override(Resources.getSystem().getDisplayMetrics().widthPixels)
                .into(holder.productImageStockout);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView productNameLabel;
        public final ImageView productImageStockout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productNameLabel = itemView.findViewById(R.id.item_pic_name);
            productImageStockout = itemView.findViewById(R.id.item_pic_image);
        }
    }
}
