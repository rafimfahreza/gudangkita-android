package prokoding.gudangkita.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.Pending;

public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.ViewHolder> {
    public OnMoreCallback onMoreCallback = new OnMoreCallback() {
        public void onClicked(Pending.Data data) {
        }
    };
    private ArrayList<Pending.Data> data = new ArrayList<>();

    public PendingAdapter(ArrayList<Pending.Data> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_pending, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.title.setText(data.get(position).refId);
        viewHolder.detail.setText(data.get(position).accessType);
        viewHolder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMoreCallback.onClicked(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public void addData(Pending.Data data) {
        this.data.add(data);
        notifyItemInserted(getItemCount());
    }

    public void setData(ArrayList<Pending.Data> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public interface OnMoreCallback {
        void onClicked(Pending.Data data);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView detail;
        public final FloatingActionButton more;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.item_title);
            detail = view.findViewById(R.id.item_detail);
            more = view.findViewById(R.id.item_more);
        }
    }
}

