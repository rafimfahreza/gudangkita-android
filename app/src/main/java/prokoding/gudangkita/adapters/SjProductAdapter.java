package prokoding.gudangkita.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.Stockout;

public class SjProductAdapter extends RecyclerView.Adapter<SjProductAdapter.ViewHolder> {
    private ArrayList<Stockout.Data.Product> data = new ArrayList<>();
    private Context context;
    private final String[] units;

    public SjProductAdapter(ArrayList<Stockout.Data.Product> data, String[] units) {
        this.data = data;
        this.units = units;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_sj_products, viewGroup, false);
        this.context = viewGroup.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (position > 0) {
            viewHolder.header.setVisibility(View.GONE);
        }
        String name = data.get(position).productName;
        String detail = String.valueOf(data.get(position).quantity);
        String unit = "-";

        for (int i = 0; i < units.length; i++) {
            String unitStr = "(" + units[i] + ")";
            if (name.contains(unitStr)) {
                name = name.replace(unitStr, "");
                unit = units[i];
                break;
            }
        }
        viewHolder.name.setText(name);
        viewHolder.unit.setText(unit);
        viewHolder.detail.setText(detail);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView name;
        public final TextView detail;
        public final TextView unit;
        public final LinearLayout header;

        public ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.item_header);
            name = view.findViewById(R.id.item_name);
            unit = view.findViewById(R.id.item_unit);
            detail = view.findViewById(R.id.item_detail);
        }
    }
}

