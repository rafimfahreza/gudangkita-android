package prokoding.gudangkita.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.Member;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {
    public OnRemoveCallback onRemoveCallback = new OnRemoveCallback() {
        public void onClicked(Member.Data data) {
        }
    };
    private ArrayList<Member.Data> data = new ArrayList<>();
    private boolean showRemoveButton = false;

    public MemberAdapter(ArrayList<Member.Data> data, boolean showRemoveButton) {
        this.data = data;
        this.showRemoveButton = showRemoveButton;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_member, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.title.setText(data.get(position).memberEmail);
        viewHolder.detail.setText(data.get(position).memberAccess);
        if (!showRemoveButton) {
            viewHolder.remove.setVisibility(View.GONE);
            return;
        }
        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRemoveCallback.onClicked(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public interface OnRemoveCallback {
        void onClicked(Member.Data data);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView detail;
        public final FloatingActionButton remove;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.item_title);
            detail = view.findViewById(R.id.item_detail);
            remove = view.findViewById(R.id.item_remove);
        }
    }
}

