package prokoding.gudangkita.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.InventoryLogs;
import prokoding.gudangkita.utils.TextUtils;

public class StockHistoryAdapter extends RecyclerView.Adapter<StockHistoryAdapter.ViewHolder> {
    private ArrayList<InventoryLogs.Data> data = new ArrayList<>();
    private Context context;

    public StockHistoryAdapter(ArrayList<InventoryLogs.Data> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_stock_history, viewGroup, false);
        this.context = viewGroup.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        boolean isIn = data.get(position).logType.equals("in");
        int quantity = data.get(position).quantity;
        viewHolder.lastStock.setText(TextUtils.simplifyAmount(data.get(position).lastStock));
        viewHolder.currentStock.setText(TextUtils.simplifyAmount(data.get(position).currentStock));
        viewHolder.member.setText(data.get(position).member);
        viewHolder.id.setText(data.get(position).refId);
        if (position == 0) {
            String date = data.get(position).logDate;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdf2 = new SimpleDateFormat("EEEE, d MMM yyyy");
                Date newDate = sdf.parse(date);
                date = sdf2.format(newDate);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                viewHolder.date.setText(date);
                viewHolder.date.setVisibility(View.VISIBLE);
            }
        } else if (position > 0) {
            if (!data.get(position - 1).logDate.equals(data.get(position).logDate)) {
                viewHolder.date.setText(data.get(position).logDate);
                viewHolder.date.setVisibility(View.VISIBLE);
            }
        }
        if (!isIn) {
            viewHolder.barIn.setVisibility(View.GONE);
            viewHolder.barOut.setVisibility(View.VISIBLE);
            viewHolder.amount.setTextColor(context.getResources().getColor(R.color.yellow));
        } else {
            viewHolder.barIn.setVisibility(View.VISIBLE);
            viewHolder.barOut.setVisibility(View.GONE);
            viewHolder.amount.setTextColor(context.getResources().getColor(R.color.green));
        }
        if (quantity >= 500000) {
            viewHolder.amount.setText(TextUtils.simplifyAmount(quantity));
        } else {
            viewHolder.amount.setText(String.valueOf(quantity));
        }

    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView amount;
        public final TextView date;
        public final TextView member;
        public final TextView id;
        public final TextView lastStock;
        public final TextView currentStock;
        public final ImageView barIn;
        public final ImageView barOut;

        public ViewHolder(View view) {
            super(view);
            id = view.findViewById(R.id.item_id);
            member = view.findViewById(R.id.item_member);
            date = view.findViewById(R.id.item_date);
            amount = view.findViewById(R.id.item_amount);
            lastStock = view.findViewById(R.id.item_last_stock);
            currentStock = view.findViewById(R.id.item_current_stock);
            barIn = view.findViewById(R.id.item_icon_in);
            barOut = view.findViewById(R.id.item_icon_out);
        }
    }
}

