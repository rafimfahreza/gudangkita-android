package prokoding.gudangkita.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.models.Stockout;
import prokoding.gudangkita.models.StockoutHistory;
import prokoding.gudangkita.models.StockoutHistoryDetail;

public class SjHistoryAdapter extends RecyclerView.Adapter<SjHistoryAdapter.ViewHolder> {
    private ArrayList<StockoutHistoryDetail.Data.Product> data = new ArrayList<>();

    public SjHistoryAdapter(ArrayList<StockoutHistoryDetail.Data.Product> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_sj_history, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (position > 0) {
            viewHolder.header.setVisibility(View.GONE);
        }
        String name = data.get(position).productName;
        String detail = data.get(position).productQuantity + " x " + data.get(position).productPrice;
        String amount = String.valueOf(data.get(position).totalAmount);
        String remaining = String.valueOf(data.get(position).remainingPaid);
        viewHolder.name.setText(name);
        viewHolder.detail.setText(detail);
        viewHolder.price.setText(amount);
        viewHolder.remaining.setText(remaining);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView name;
        public final TextView detail;
        public final TextView price;
        public final TextView remaining;
        public final LinearLayout header;

        public ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.item_header);
            name = view.findViewById(R.id.item_name);
            detail = view.findViewById(R.id.item_detail);
            price = view.findViewById(R.id.item_price);
            remaining = view.findViewById(R.id.item_remaining);
        }
    }
}

