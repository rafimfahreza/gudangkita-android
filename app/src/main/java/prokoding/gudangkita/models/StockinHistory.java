package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class StockinHistory extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public int stockinNumber;
        public String stockinId;
        public int productId;
        public int productType;
        public int warehouseId;
        public int quantity;
        public String type;
        public String productName;
        public String warehouseName;
        public String memberEmail;
        public String PONumber;
    }
}
