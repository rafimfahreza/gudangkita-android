package prokoding.gudangkita.models;

import java.io.Serializable;

public class PendingCustomer extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public String customerName;
        public String customerPhone;
        public String customerIdentityType;
        public String customerIdentityNumber;
        public String customerIdentityPic;
        public String customerAddress;
        public String province;
        public String customerType;
    }
}
