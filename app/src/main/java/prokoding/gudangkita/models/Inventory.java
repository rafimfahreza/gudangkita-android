package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Inventory extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public int productNumber;
        public String productName;
        public String productCode;
        public int baseStock;
        public String productImage;
    }
}
