package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public int customerNumber;
        public String customerId;
        public String customerName;
        public String customerType;
        public int isApproved;
    }

}
