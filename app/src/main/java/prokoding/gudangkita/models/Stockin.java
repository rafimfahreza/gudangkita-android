package prokoding.gudangkita.models;

import java.io.Serializable;

public class Stockin extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public int stockId;
        public String stockRefId;
        public int stockInQuantity;
        public String lastUpdate;
        public String addedByMemberId;
    }
}
