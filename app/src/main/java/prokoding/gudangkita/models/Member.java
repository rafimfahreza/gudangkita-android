package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Member extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public String memberEmail;
        public String memberAccess;
    }
}
