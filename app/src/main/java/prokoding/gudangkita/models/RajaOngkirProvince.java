package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class RajaOngkirProvince implements Serializable {
    public Rajaongkir rajaongkir;

    public static class Rajaongkir {
        public Query query;
        public Status status;
        public ArrayList<Data> results;

        public static class Query {
            public String key;
        }

        public static class Status {
            public int code;
            public String description;
        }

        public static class Data {
            public String provinceId;
            public String province;
        }
    }
}
