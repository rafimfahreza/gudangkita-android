package prokoding.gudangkita.models;

import java.io.Serializable;

public class PendingStockin extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public int refNumber;
        public String refId;
        public int productId;
        public int quantity;
        public int warehouseId;
        public String lastUpdated;
        public int memberId;
        public String requestType;
        public String productaName;
        public String warehouseName;
        public String memberEmail;

    }
}
