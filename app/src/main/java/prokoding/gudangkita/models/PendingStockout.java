package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class PendingStockout extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public String stockoutId;
        public int customerId;
        public String customerName;
        public int productId;
        public String productName;
        public int productQuantity;
        public String stockoutDate;
        public String stockouType;
        public String memberName;
        public String paymentType;
        public int stockoutNumber;
        public String deliveryDate;
    }
}
