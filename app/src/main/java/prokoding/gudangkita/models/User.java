package prokoding.gudangkita.models;

import java.io.Serializable;

public class User extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public String mailAccount;
        public int userId;
        public int accountType;
        public boolean isActive;
        public String token;
    }
}
