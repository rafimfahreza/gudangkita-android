package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Notification extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public String refId;
        public boolean isDone;
    }
}
