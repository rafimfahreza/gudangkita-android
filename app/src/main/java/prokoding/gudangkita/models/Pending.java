package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Pending extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public String refId;
        public String requestType;
        public int requestNumber;
        public String accessType;
    }
}
