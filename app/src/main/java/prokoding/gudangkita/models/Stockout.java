package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Stockout extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public String sjNumber;
        public int customerId;
        public int warehouseId;
        public String paymentType;
        public int stockoutType;
        public String orderDate;
        public String customerName;
        public String address;
        public int addedByMemberId;
        public ArrayList<Product> products;

        public static class Product implements Serializable {
            public String productName;
            public String productCode;
            public int stockoutIdNumber;
            public String subSjNumber;
            public int quantity;
        }
    }
}
