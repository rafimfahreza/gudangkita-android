package prokoding.gudangkita.models;

import java.util.ArrayList;

public class StockoutHistory extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data {
        public String stockoutId;
        public String memberName;
        public String date;
        public String paymentType;
    }
}
