package prokoding.gudangkita.models;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    public String responseCode;
    public String responseMessage;
    public String apiVersion;
    public String minimumAppVersion;
}
