package prokoding.gudangkita.models;

import java.io.Serializable;

public class InventoryUpdate extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public int updatedId;
    }
}
