package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class InventoryLogs extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public int quantity;
        public String logType;
        public int lastStock;
        public int currentStock;
        public String logDate;
        public String refId;
        public String member;
        public int warehouseId;
    }
}
