package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class StockoutHistoryDetail extends BaseResponse {
    public Data data;
    public static class Data implements Serializable {
        public String sjNumber;
        public String sjPicture;
        public String receiver;
        public String deliveryDate;
        public String orderDate;
        public String emailMember;
        public String deadline;
        public ArrayList<Product> products;
        public static class Product implements Serializable {
            public String productName;
            public int productQuantity;
            public int productPrice;
            public int totalAmount;
            public int remainingPaid;
            public String packagePicture;
        }
    }
}
