package prokoding.gudangkita.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Warehouse extends BaseResponse {
    public ArrayList<Data> data;

    public static class Data implements Serializable {
        public int warehouseNumber;
        public String warehouseCode;
        public String warehouseName;
    }
}
