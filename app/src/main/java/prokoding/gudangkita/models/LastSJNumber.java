package prokoding.gudangkita.models;

import java.io.Serializable;

public class LastSJNumber extends BaseResponse {
    public Data data;

    public static class Data implements Serializable {
        public String lastSjNumber;
    }
}
