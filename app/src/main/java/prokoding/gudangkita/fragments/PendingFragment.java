package prokoding.gudangkita.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.PendingCustomerActivity;
import prokoding.gudangkita.activities.PendingStockinActivity;
import prokoding.gudangkita.activities.PendingStockoutActivity;
import prokoding.gudangkita.adapters.PendingAdapter;
import prokoding.gudangkita.models.Pending;

public class PendingFragment extends Fragment {
    private static final String KEY_DATA = "DATA";
    private final String type;
    public Callback callback = new Callback() {
        @Override
        public void onBack() {

        }
    };
    private ArrayList<Pending.Data> data = new ArrayList<>();
    private final PendingAdapter adapter = new PendingAdapter(data);
    private RecyclerView recyclerView;
    private NestedScrollView noData;
    private final ActivityResultLauncher<Intent> pendingDetailLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    callback.onBack();
                }
            }
    );

    public PendingFragment(String type) {
        this.type = type;
    }

    public PendingFragment(String type, Callback callback) {
        this.type = type;
        this.callback = callback;
    }

    public static PendingFragment newInstance(String type) {
        PendingFragment fragment = new PendingFragment(type);
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATA, new ArrayList<>());
        fragment.setArguments(args);
        return fragment;
    }

    public static PendingFragment newInstance(String type, Callback callback) {
        PendingFragment fragment = newInstance(type);
        fragment.callback = callback;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pending, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            this.data = (ArrayList<Pending.Data>) getArguments().getSerializable(KEY_DATA);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        noData = view.findViewById(R.id.pending_nodata);
        recyclerView = view.findViewById(R.id.pending_list);
        adapter.setData(data);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        if (this.data.size() <= 0) {
            recyclerView.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
            return;
        }
        recyclerView.setVisibility(View.VISIBLE);
        noData.setVisibility(View.GONE);
        if (this.type.equals("in")) {
            adapter.onMoreCallback = new PendingAdapter.OnMoreCallback() {
                @Override
                public void onClicked(Pending.Data data) {
                    Intent intent = new Intent(getContext(), PendingStockinActivity.class);
                    intent.putExtra(PendingStockinActivity.ARG_REF_ID, data.refId);
                    pendingDetailLauncher.launch(intent);
                    getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                }
            };
            return;
        }
        if (this.type.equals("cust")) {
            adapter.onMoreCallback = new PendingAdapter.OnMoreCallback() {
                @Override
                public void onClicked(Pending.Data data) {
                    Intent intent = new Intent(getContext(), PendingCustomerActivity.class);
                    intent.putExtra(PendingCustomerActivity.ARG_REF_ID, data.refId);
                    pendingDetailLauncher.launch(intent);
                    getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                }
            };
            return;
        }
        if (this.type.equals("out")) {
            adapter.onMoreCallback = new PendingAdapter.OnMoreCallback() {
                @Override
                public void onClicked(Pending.Data data) {
                    Intent intent = new Intent(getContext(), PendingStockoutActivity.class);
                    intent.putExtra(PendingStockoutActivity.ARG_REF_ID, data.refId);
                    pendingDetailLauncher.launch(intent);
                    getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                }
            };
            return;
        }
    }

    public void addData(Pending.Data data) {
        Bundle args = new Bundle();
        this.data.add(data);
        args.putSerializable(KEY_DATA, this.data);
        setArguments(args);
    }

    public interface Callback {
        void onBack();
    }
}