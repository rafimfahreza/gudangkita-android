package prokoding.gudangkita.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import prokoding.gudangkita.adapters.StockHistoryAdapter;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.InventoryLogs;
import prokoding.gudangkita.models.StockoutHistoryDetail;

import java.util.ArrayList;

import prokoding.gudangkita.R;

public class InventoryLogFragment extends Fragment {
    ArrayList<InventoryLogs.Data> logs;
    View noData;
    RecyclerView recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        container = new FrameLayout(getContext());
        recyclerView = (RecyclerView) inflater.inflate(R.layout.activity_inventory_detail_history, null);
        noData = inflater.inflate(R.layout.component_nodata, null);
        container.addView(recyclerView);
        container.addView(noData);
        return container;
    }

    public InventoryLogFragment(ArrayList<InventoryLogs.Data> logs) {
        this.logs = logs;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        noData.setVisibility(View.GONE);
        if (logs.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            StockHistoryAdapter adapter = new StockHistoryAdapter(logs);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        noData.setVisibility(View.VISIBLE);
    }
}