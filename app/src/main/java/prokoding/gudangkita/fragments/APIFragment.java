package prokoding.gudangkita.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import prokoding.gudangkita.R;

public abstract class APIFragment extends Fragment {
    public static int STATE_SHIVER = 1;
    public static int STATE_CONTENT = 2;
    public static int STATE_NODATA = 3;
    public static int STATE_FAILED = 4;
    private View nodata;
    private View failed;
    private View shiver;
    private View content;
    private Button reload;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        container = new FrameLayout(getContext());
        nodata = inflater.inflate(R.layout.component_nodata, null);
        failed = inflater.inflate(R.layout.component_reload, null);
        content = inflater.inflate(getContentLayout(), null);
        shiver = inflater.inflate(getShiverLayout(), null);
        container.addView(nodata);
        container.addView(failed);
        container.addView(content);
        container.addView(shiver);
        reload = failed.findViewById(R.id.reload_button);
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleReload();
            }
        });
        switchLayoutState(STATE_CONTENT);
        return container;
    }

    protected abstract @LayoutRes
    int getContentLayout();

    protected @LayoutRes
    int getShiverLayout() {
        return R.layout.component_item_list_shiver;
    }

    protected abstract void handleReload();

    protected void switchLayoutState(int state) {
        if (state == STATE_SHIVER) {
            shiver.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            failed.setVisibility(View.GONE);
            nodata.setVisibility(View.GONE);
            return;
        }
        if (state == STATE_CONTENT) {
            shiver.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            failed.setVisibility(View.GONE);
            nodata.setVisibility(View.GONE);
            return;
        }
        if (state == STATE_FAILED) {
            shiver.setVisibility(View.GONE);
            content.setVisibility(View.GONE);
            failed.setVisibility(View.VISIBLE);
            nodata.setVisibility(View.GONE);
            return;
        }
        if (state == STATE_NODATA) {
            shiver.setVisibility(View.GONE);
            content.setVisibility(View.GONE);
            failed.setVisibility(View.GONE);
            nodata.setVisibility(View.VISIBLE);
            return;
        }
    }


}