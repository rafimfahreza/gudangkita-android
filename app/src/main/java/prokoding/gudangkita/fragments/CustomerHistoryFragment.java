package prokoding.gudangkita.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.CustomerUpdateActivity;
import prokoding.gudangkita.adapters.HistoryAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.Customer;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerHistoryFragment extends APIFragment {
    private RecyclerView recyclerView;
    private final HistoryAdapter adapter = new HistoryAdapter();
    private LinearLayoutManager linearLayoutManager;
    private final ActivityResultLauncher<Intent> launcher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    handlePrefetchData();
                }
            }
    );

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_pending;
    }

    protected void handleReload() {
        handlePrefetchData();
    }

    protected void handlePrefetchData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(getContext());
        String words = AuthUtils.getWords(getContext());
        String token = AuthUtils.getToken(getContext());
        Call<Customer> call = ApiRetrofit.getApi(getContext()).getAllCustomers(appVersion, words, token);
        call.enqueue(new Callback<Customer>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                if (response.body() != null) {
                    if (response.body().data != null) {
                        switchLayoutState(STATE_CONTENT);
                        handleOnResponse(response.body().data);
                        return;
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(getActivity(),AuthUtils.getCurrentUser(getActivity()).mailAccount,response.body().responseMessage, "cust_hist_prefetch_failed");

                    }
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleOnResponse(ArrayList<Customer.Data> data) {
        if (data.size() > 0) {
            switchLayoutState(STATE_CONTENT);
            adapter.clearData();
            for (int i = 0; i < data.size(); i++) {
                HistoryAdapter.Data adapterData = new HistoryAdapter.Data();
                adapterData.title = data.get(i).customerName;
                adapterData.detail = data.get(i).customerType;
                adapter.addData(adapterData);
            }
            adapter.callback = new HistoryAdapter.Callback() {
                @Override
                public void onMoreClicked(HistoryAdapter.Data historyData, int position) {
                    Intent intent = new Intent(getContext(), CustomerUpdateActivity.class);
                    intent.putExtra(CustomerUpdateActivity.ARG_CUSTOMER, data.get(position).customerId);
                    intent.putExtra(CustomerUpdateActivity.ARG_APPROVED, data.get(position).isApproved);
                    launcher.launch(intent);
                }
            };
            return;
        }
        switchLayoutState(STATE_NODATA);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.pending_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(getActivity(),AuthUtils.getCurrentUser(getActivity()).mailAccount,"", "customer_history_view");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        container = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        handlePrefetchData();
        return container;
    }

}