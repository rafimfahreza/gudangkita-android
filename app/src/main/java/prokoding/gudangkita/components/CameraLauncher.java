package prokoding.gudangkita.components;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;

import prokoding.gudangkita.R;
import prokoding.gudangkita.utils.DrawableUtils;
import prokoding.gudangkita.utils.ProgressDialogUtils;

public class CameraLauncher extends MaterialCardView {
    private MaterialCardView card;
    private LinearLayout upload;
    private ConstraintLayout preview;
    private ImageView image;
    private FloatingActionButton remove;
    private String filePath;
    private Uri fileUri;
    ActivityResultLauncher<Intent> permissionAskIntent = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (Environment.isExternalStorageManager()) {
                            triggerCameraPicker();
                        }
                    }
                }
            }
    );
    ActivityResultLauncher<String> permissionAskLauncher = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    if (result) {
                        triggerCameraPicker();
                    }
                }
            }
    );
    private OnCameraCaptured onCameraCaptured = new OnCameraCaptured() {
        @Override
        public void onCameraCaptured(String filepath) {
        }
    };
    ActivityResultLauncher<Intent> cameraLauncher = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        try {
                            Bitmap photo = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), fileUri);
                            Bitmap resize = DrawableUtils.resizeBitmap(photo, filePath, 512);
                            DrawableUtils.writeBitmapToTemp(resize, "camera");
                            image.setImageBitmap(resize);
                            upload.setVisibility(GONE);
                            preview.setVisibility(VISIBLE);
                            onCameraCaptured.onCameraCaptured(filePath);
                        } catch (Exception e) {
                            removeImage();
                            e.printStackTrace();
                        }

                    }
                }
            }
    );

    public CameraLauncher(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        boolean enabled = attributeSet.getAttributeBooleanValue("android", "enabled", true);
        inflate(getContext(), R.layout.component_camera_launcher, this);
        card = findViewById(R.id.component_card);
        upload = findViewById(R.id.component_upload);
        preview = findViewById(R.id.component_preview);
        image = findViewById(R.id.component_image);
        remove = findViewById(R.id.component_remove);
        setEnabled(enabled);
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            remove.setVisibility(VISIBLE);
            card.setFocusable(true);
            card.setClickable(true);
            card.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    triggerCameraPicker();
                }
            });
            remove.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeImage();
                }
            });
        } else {
            remove.setVisibility(GONE);
            card.setOnClickListener(null);
            card.setFocusable(false);
            card.setClickable(false);
            remove.setOnClickListener(null);
        }
    }

    public void triggerCameraPicker() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            int cameraPermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                intent.setData(uri);
                Toast.makeText(getContext(), R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskIntent.launch(intent);
                return;
            }
            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissionAskLauncher.launch(Manifest.permission.CAMERA);
                return;
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int cameraPermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
            int writePermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissionAskLauncher.launch(Manifest.permission.CAMERA);
                return;
            }
            if (writePermission != PackageManager.PERMISSION_GRANTED) {
                permissionAskLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                return;
            }
        }
        File file = new File(Environment.getExternalStorageDirectory(), "/GudangKita/.temp/camera.jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GudangKita/.temp/camera.jpg";
        fileUri = FileProvider.getUriForFile(getContext(), "prokoding.gudangkita.provider", file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        cameraLauncher.launch(intent);
    }

    public void setOnCameraCaptured(OnCameraCaptured onCameraCaptured) {
        this.onCameraCaptured = onCameraCaptured;
    }

    public String getFilePath() {
        return filePath;
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public void removeImage() {
        preview.setVisibility(GONE);
        upload.setVisibility(VISIBLE);
        image.setImageBitmap(null);
        File file = new File(Environment.getExternalStorageDirectory(), "/GudangKita/.temp/camera.jpg");
        file.delete();
        filePath = null;
        fileUri = null;
    }

    public interface OnCameraCaptured {
        void onCameraCaptured(String filepath);
    }

}
