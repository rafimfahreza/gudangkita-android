package prokoding.gudangkita.components;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.o7planning.filechooserexample.FileUtils;

import java.io.File;

import prokoding.gudangkita.R;
import prokoding.gudangkita.utils.DrawableUtils;

public class ImagePicker extends MaterialCardView {
    ActivityResultLauncher<Intent> permissionAskIntent = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (Environment.isExternalStorageManager()) {
                            triggerFilePicker();
                        }
                    }
                }
            }
    );
    ActivityResultLauncher<String> permissionAskLauncher = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    if (result) {
                        triggerFilePicker();
                    }
                }
            }
    );
    private MaterialCardView card;
    private LinearLayout upload;
    private ConstraintLayout preview;
    private ImageView image;
    private FloatingActionButton remove;
    private String filePath;
    private Uri fileUri;
    private OnFilePicked onFilePicked = new OnFilePicked() {
        @Override
        public void onFilePicked(String filepath) {
        }
    };
    ActivityResultLauncher<String> filePickerLauncher = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.GetContent(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri URL) {
                    if (URL != null) {
                        try {
                            removeImage();
                            Bitmap photo = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), URL);
                            Bitmap resize = DrawableUtils.resizeBitmap(photo, FileUtils.getPath(getContext(), URL), 512);
                            filePath = DrawableUtils.writeBitmapToTemp(resize, "image_picker");
                            fileUri = FileProvider.getUriForFile(getContext(), "prokoding.gudangkita.provider", new File(filePath));
                            updateImage(resize);
                            onFilePicked.onFilePicked(filePath);
                        } catch (Exception e) {
                            removeImage();
                            e.printStackTrace();
                        }
                    }
                }
            }
    );

    public ImagePicker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public void setImageByUrl(String url) {
        setImageByUrl(url, true);
    }

    public void setImageByUrl(String url, boolean isEnabled) {
        try {
            setEnabled(!isEnabled);
            removeImage();
            Glide.with(getContext())
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(new CustomTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                            filePath = DrawableUtils.writeBitmapToTemp(bitmap, "image_picker");
                            fileUri = FileProvider.getUriForFile(getContext(), "prokoding.gudangkita.provider", new File(filePath));
                            Bitmap resize = DrawableUtils.resizeBitmap(bitmap, filePath, 512);
                            DrawableUtils.writeBitmapToTemp(resize, "image_picker");
                            updateImage(resize);
                            onFilePicked.onFilePicked(filePath);
                            setEnabled(isEnabled);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            setEnabled(isEnabled);
                            removeImage();
                            onFilePicked.onFilePicked(filePath);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            setEnabled(isEnabled);
            removeImage();
            onFilePicked.onFilePicked(filePath);
        }

    }

    private void init(AttributeSet attributeSet) {
        boolean enabled = attributeSet.getAttributeBooleanValue("android", "enabled", true);
        inflate(getContext(), R.layout.component_image_picker, this);
        card = findViewById(R.id.component_card);
        upload = findViewById(R.id.component_upload);
        preview = findViewById(R.id.component_preview);
        image = findViewById(R.id.component_image);
        remove = findViewById(R.id.component_remove);
        setEnabled(enabled);
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            remove.setVisibility(VISIBLE);
            card.setFocusable(true);
            card.setClickable(true);
            card.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    triggerFilePicker();
                }
            });
            remove.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeImage();
                }
            });
        } else {
            remove.setVisibility(GONE);
            card.setOnClickListener(null);
            card.setFocusable(false);
            card.setClickable(false);
            remove.setOnClickListener(null);
        }
    }

    public String getFilePath() {
        return filePath;
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public void triggerFilePicker() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                intent.setData(uri);
                Toast.makeText(getContext(), R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskIntent.launch(intent);
                return;
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionRead = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            int permissionWrite = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionRead != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
                return;
            }
            if (permissionWrite != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                return;
            }
        }
        filePickerLauncher.launch("image/*");
    }

    public void setOnFilePicked(OnFilePicked onFilePicked) {
        this.onFilePicked = onFilePicked;
    }

    public void removeImage() {
        if (filePath != null) {
            if (filePath.contains(".temp")) {
                File file = new File(filePath);
                file.delete();
            }
        }
        preview.setVisibility(GONE);
        upload.setVisibility(VISIBLE);
        filePath = null;
        fileUri = null;
        onFilePicked.onFilePicked(filePath);
    }

    void updateImage(Bitmap bitmap) {
        try {
            image.setImageBitmap(bitmap);
            upload.setVisibility(GONE);
            preview.setVisibility(VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
            removeImage();
        }
    }

    public interface OnFilePicked {
        void onFilePicked(String filepath);
    }


}
