package prokoding.gudangkita.components;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import prokoding.gudangkita.R;

public class Dialog extends android.app.Dialog {
    public Callback callback = new Callback() {
        @Override
        public void onPositive() {
        }
    };
    private final View.OnClickListener onCancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Dialog.this.dismiss();
        }
    };
    private final View.OnClickListener onPositive = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callback.onPositive();
            Dialog.this.dismiss();
        }
    };

    public Dialog(Context context, String title, String message) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.component_dialog);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.getWindow().setBackgroundDrawableResource(R.color.transparent);
        this.getWindow().setDimAmount(0.85f);
        if (message == null) {
            ((TextView) this.findViewById(R.id.dialog_message)).setText("");
        } else {
            ((TextView) this.findViewById(R.id.dialog_message)).setText(message);
        }
        if (title == null) {
            ((TextView) this.findViewById(R.id.dialog_title)).setText("");
        } else {
            ((TextView) this.findViewById(R.id.dialog_title)).setText(title);
        }
        ((MaterialButton) this.findViewById(R.id.dialog_cancel)).setOnClickListener(onCancel);
        ((MaterialButton) this.findViewById(R.id.dialog_submit)).setOnClickListener(onPositive);
        ((FloatingActionButton) this.findViewById(R.id.dialog_close)).setOnClickListener(onCancel);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public interface Callback {
        void onPositive();
    }
}