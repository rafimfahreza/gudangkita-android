package prokoding.gudangkita.components;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import prokoding.gudangkita.R;

public class RemovalDialog extends Dialog {
    public Callback callback = new Callback() {
        @Override
        public void onRemove() {
        }
    };
    private final View.OnClickListener onCancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RemovalDialog.this.dismiss();
        }
    };

    public RemovalDialog(Context context, String message, String validation) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.component_removal_dialog);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.getWindow().setBackgroundDrawableResource(R.color.transparent);
        this.getWindow().setDimAmount(0.85f);
        if (message == null) {
            ((TextView) this.findViewById(R.id.dialog_message)).setText(context.getString(R.string.hint_loading));
        } else {
            ((TextView) this.findViewById(R.id.dialog_message)).setText(message);
        }
        ((MaterialButton) this.findViewById(R.id.dialog_cancel)).setOnClickListener(onCancel);
        ((FloatingActionButton) this.findViewById(R.id.dialog_close)).setOnClickListener(onCancel);
        ((TextInputEditText) this.findViewById(R.id.dialog_input)).setHint(validation);

        ((TextInputEditText) this.findViewById(R.id.dialog_input)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                onInputChange(validation);
            }
        });
    }

    protected void onInputChange(String validation) {
        TextInputEditText txEdit = this.findViewById(R.id.dialog_input);
        MaterialButton submit = this.findViewById(R.id.dialog_submit);
        submit.setEnabled(txEdit.getText().toString().equals(validation));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public interface Callback {
        void onRemove();
    }
}