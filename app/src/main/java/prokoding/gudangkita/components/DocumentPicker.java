package prokoding.gudangkita.components;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.o7planning.filechooserexample.FileUtils;

import prokoding.gudangkita.R;

public class DocumentPicker extends MaterialCardView {
    MaterialCardView card;
    LinearLayout upload;
    ConstraintLayout preview;
    TextView tvFilePath;
    String filePath;
    Uri fileUri;
    FloatingActionButton remove;
    OnFilePicked onFilePicked = new OnFilePicked() {
        @Override
        public void onFilePicked(String filepath) {
        }
    };
    ActivityResultLauncher<String[]> filePickerLauncher = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.OpenDocument(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri URL) {
                    if (URL != null) {
                        filePath = FileUtils.getPath(getContext(), URL);
                        fileUri = URL;
                        Log.i("FileUtils", "URI: " + filePath);
                        updatePreview();
                        onFilePicked.onFilePicked(filePath);
                    }
                }
            }
    );
    ActivityResultLauncher<Intent> permissionAskIntent = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (Environment.isExternalStorageManager()) {
                            triggerFilePicker();
                        }
                    }
                }
            }
    );
    ActivityResultLauncher<String> permissionAskLauncher = ((AppCompatActivity) getContext()).registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    if (result) {
                        triggerFilePicker();
                    }
                }
            }
    );

    public DocumentPicker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    private void init(Context context, AttributeSet attributeSet) {
        inflate(context, R.layout.component_document_picker, this);
        card = findViewById(R.id.component_card);
        upload = findViewById(R.id.component_upload);
        preview = findViewById(R.id.component_preview);
        tvFilePath = findViewById(R.id.component_filepath);
        remove = findViewById(R.id.component_remove);
        card.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                triggerFilePicker();
            }
        });
        remove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDocument();
            }
        });
    }

    public String getFilePath() {
        return filePath;
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public void triggerFilePicker() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                intent.setData(uri);
                Toast.makeText(getContext(), R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskIntent.launch(intent);
                return;
            }

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (permission != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
                return;
            }
        }
        filePickerLauncher.launch(new String[]{"application/pdf", "image/jpeg"});
    }

    public void setOnFilePicked(OnFilePicked onFilePicked) {
        this.onFilePicked = onFilePicked;
    }

    void removeDocument() {
        tvFilePath.setText("");
        filePath = null;
        fileUri = null;
        upload.setVisibility(VISIBLE);
        preview.setVisibility(GONE);
    }

    void updatePreview() {
        if (filePath != null) {
            tvFilePath.setText(filePath);
            upload.setVisibility(GONE);
            preview.setVisibility(VISIBLE);
            return;
        }
        removeDocument();
    }

    public interface OnFilePicked {
        void onFilePicked(String filepath);
    }


}
