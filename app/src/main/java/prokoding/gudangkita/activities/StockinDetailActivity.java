package prokoding.gudangkita.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.ArrayList;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.DocumentPicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.PendingStockin;
import prokoding.gudangkita.models.StockinHistory;
import prokoding.gudangkita.models.Warehouse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockinDetailActivity extends SubActivity {
    public static String ARG_DATA = "DATA";
    private AutoCompleteTextView id;
    private AutoCompleteTextView product;
    private AutoCompleteTextView warehouseLocation;
    private TextInputEditText quantity;
    private StockinHistory.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.stockin_title));
        handleMapComponents();
        handleFillData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(StockinDetailActivity.this,AuthUtils.getCurrentUser(StockinDetailActivity.this).mailAccount,"", "stockin_detail_view");

    }



    protected void handleMapComponents() {
        inflateLayout(R.layout.activity_stockin_detail);
        id = findViewById(R.id.stockin_id);
        product = findViewById(R.id.stockin_product);
        warehouseLocation = findViewById(R.id.stockin_warehouse_location);
        quantity = findViewById(R.id.stockin_quantity);
    }

    protected void handleFillData() {
        data = (StockinHistory.Data) getIntent().getSerializableExtra(ARG_DATA);
        id.setText(data.stockinId);
        product.setText(data.productName);
        quantity.setText(String.valueOf(data.quantity));
        warehouseLocation.setText(data.warehouseName);
    }
}