package prokoding.gudangkita.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.Arrays;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryAddActivity extends SubActivity {
    private TextInputEditText itemCode;
    private TextInputEditText itemName;
    private TextInputEditText baseStock;
    private AutoCompleteTextView itemType;
    private ImagePicker imagePicker1;
    private MaterialButton submit;
    private String[] optItemType;
    private boolean isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateLayout(R.layout.activity_inventory_add);
        setTitle(getString(R.string.inventories_title));
        handleMapComponents();
        handleFillData();
        handleBindEventValidate();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(InventoryAddActivity.this,AuthUtils.getCurrentUser(InventoryAddActivity.this).mailAccount,"", "add_inventory_view");

    }

    protected void handleMapComponents() {
        isAdmin = AuthUtils.getCurrentUser(this).accountType >= 3;
        itemCode = findViewById(R.id.inventoryadd_item_code);
        itemName = findViewById(R.id.inventoryadd_item_name);
        itemType = findViewById(R.id.inventoryadd_product_type);
        baseStock = findViewById(R.id.inventoryadd_item_base_stock);
        imagePicker1 = findViewById(R.id.inventoryadd_upload_1);
        submit = findViewById(R.id.inventoryadd_submit);
        if (!isAdmin) {
            finish();
        }
    }

    protected void handleFillData() {
        optItemType = getResources().getStringArray(R.array.value_item_type);
        ArrayAdapter<String> productTypeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optItemType);
        itemType.setAdapter(productTypeAdapter);
    }

    protected void handleBindEventValidate() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                handleValidate();
            }
        };
        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };
        ImagePicker.OnFilePicked onFilePicked = new ImagePicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filePath) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        };
        itemCode.addTextChangedListener(onTextChange);
        itemName.addTextChangedListener(onTextChange);
        baseStock.addTextChangedListener(onTextChange);
        itemType.setOnItemClickListener(onDropdownChange);
        imagePicker1.setOnFilePicked(onFilePicked);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleSubmit() {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(InventoryAddActivity.this,AuthUtils.getCurrentUser(InventoryAddActivity.this).mailAccount,"", "add_inventory_clicked");
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        File file = new File(imagePicker1.getFilePath());
        ProgressDialogUtils.show(InventoryAddActivity.this);
        Call<BaseResponse> callStockin = ApiRetrofit.getApi(InventoryAddActivity.this).addProduct(
                RequestBody.create(MultipartBody.FORM, appVersion),
                RequestBody.create(MultipartBody.FORM, words),
                RequestBody.create(MultipartBody.FORM, token),
                RequestBody.create(MultipartBody.FORM, itemCode.getText().toString()),
                RequestBody.create(MultipartBody.FORM, itemName.getText().toString()),
                RequestBody.create(MultipartBody.FORM, String.valueOf(Arrays.asList(optItemType).indexOf(itemType.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, baseStock.getText().toString()),
                MultipartBody.Part.createFormData("itemPicture", file.getName(),
                        RequestBody.create(
                                MediaType.parse(imagePicker1.getFilePath()),
                                file
                        )
                )
        );
        callStockin.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().responseCode != null) {
                    if (response.body().responseCode.equals("1433")) {
                        StyleableToast.makeText(InventoryAddActivity.this, String.format("Item with code %s was already exist", itemCode.getText().toString()), R.style.Theme_GudangKita_Toast_Danger).show();
                        return;
                    }
                    handleClear();
                    StyleableToast.makeText(InventoryAddActivity.this, "Item successfully addded", R.style.Theme_GudangKita_Toast_Success).show();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(InventoryAddActivity.this,AuthUtils.getCurrentUser(InventoryAddActivity.this).mailAccount,response.body().responseMessage, "add_inventory_failed");
                }
                StyleableToast.makeText(InventoryAddActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                StyleableToast.makeText(InventoryAddActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                ProgressDialogUtils.dismiss();
            }
        });
    }

    protected void handleClear() {
        itemCode.setText("");
        itemName.setText("");
        baseStock.setText("");
        itemType.setText("", false);
        imagePicker1.removeImage();
        handleValidate();
    }

    protected void handleValidate() {
        int baseStockNum;
        try {
            baseStockNum = Integer.valueOf(baseStock.getText().toString());
        } catch (Exception e) {
            baseStockNum = 0;
        }
        boolean valid = !itemName.getText().toString().isEmpty() &&
                Arrays.asList(optItemType).contains(itemType.getText().toString()) &&
                baseStockNum >= 0 &&
                imagePicker1.getFilePath() != null;
        submit.setEnabled(valid);
    }

}