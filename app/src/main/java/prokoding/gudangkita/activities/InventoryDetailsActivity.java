package prokoding.gudangkita.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.adapters.PageAdapter;
import prokoding.gudangkita.adapters.PendingPageAdapter;
import prokoding.gudangkita.adapters.StockHistoryAdapter;
import prokoding.gudangkita.adapters.TabAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.fragments.InventoryLogFragment;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.InventoryLogs;
import prokoding.gudangkita.models.Warehouse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.TextUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryDetailsActivity extends SubAPIActivity {
    private TextView tvHeader;
    private TextView tvTotal;
    private RecyclerView historyList;
    private ViewGroup container;
    private RecyclerView categories;
    private ArrayList<InventoryLogs.Data> data;
    private ArrayList<Warehouse.Data> warehouses;
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private ViewPager2 viewPager2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.inventories_title));
        handleMapComponents();
        handlePrefetchData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(InventoryDetailsActivity.this, AuthUtils.getCurrentUser(InventoryDetailsActivity.this).mailAccount, "", "add_inventory_view");
    }

    @Override
    protected ViewGroup createContainer() {
        return findViewById(R.id.inventory_detail_history_container);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(R.layout.component_item_card_shiver, null);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_inventory_detail_history2, null);
    }

    @Override
    protected void handleReload() {
        handlePrefetchData();
    }

    @Override
    protected void initLayoutState() {
        container = findViewById(R.id.nonstockout_container);
        container.addView(getLayoutInflater().inflate(R.layout.activity_inventory_detail, null));
        super.initLayoutState();
    }

    protected void handleMapComponents() {
        tvHeader = findViewById(R.id.inventory_detail_title);
        tvTotal = findViewById(R.id.inventory_detail_total);
        viewPager2 = findViewById(R.id.inventory_detail_history);
        categories = findViewById(R.id.inventory_detail_history_categories);
    }

    protected void handlePrefetchData() {
        Intent intent = getIntent();
        Inventory.Data inventory = (Inventory.Data) intent.getSerializableExtra("data");
        tvHeader.setText(inventory.productName);
        if (inventory.baseStock >= 500000) {
            tvTotal.setText(TextUtils.simplifyAmount(inventory.baseStock));
        } else {
            tvTotal.setText(String.valueOf(inventory.baseStock));
        }
        switchLayoutState(STATE_SHIVER);
        categories.setVisibility(View.GONE);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<InventoryLogs> call = ApiRetrofit.getApi(this).getProductLog(appVersion, words, token, inventory.productNumber);
        Call<Warehouse> callWarehouse = ApiRetrofit.getApi(this).getWarehouses(appVersion, words, token);
        call.enqueue(new Callback<InventoryLogs>() {
            @Override
            public void onResponse(Call<InventoryLogs> call, Response<InventoryLogs> response) {
                if (response.body().data != null) {
                    data = response.body().data;
                    handleResponse();
                    return;
//                    ArrayList<InventoryLogs.Data> data = response.body().data;
//                    if (data.size() <= 0) {
//                        switchLayoutState(STATE_NODATA);
//                        return;
//                    }
//                    categories.setVisibility(View.VISIBLE);
//                    StockHistoryAdapter historyAdapter = new StockHistoryAdapter(data);
//                    LinearLayoutManager historyLayoutManager = new LinearLayoutManager(InventoryDetailsActivity.this);
//                    historyList.setLayoutManager(historyLayoutManager);
//                    historyList.setAdapter(historyAdapter);
//                    switchLayoutState(STATE_CONTENT);
//                    return;
                }
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(InventoryDetailsActivity.this, AuthUtils.getCurrentUser(InventoryDetailsActivity.this).mailAccount, response.body().responseMessage, "stock_log_failed");
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<InventoryLogs> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });

        callWarehouse.enqueue(new Callback<Warehouse>() {
            @Override
            public void onResponse(Call<Warehouse> call, Response<Warehouse> response) {
                if (response.body().data != null) {
                    warehouses = response.body().data;
                    handleResponse();
                    return;
                }
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(InventoryDetailsActivity.this, AuthUtils.getCurrentUser(InventoryDetailsActivity.this).mailAccount, response.body().responseMessage, "warehouses_get_failed");
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Warehouse> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleResponse() {
        if (data != null && warehouses != null) {
            PageAdapter pageAdapter;
            TabAdapter tabAdapter;
            ArrayList<String> tabLabels = new ArrayList<>();
            tabLabels.add("All");
            fragments.add(new InventoryLogFragment(data));
            for (int i = 0; i < warehouses.size(); i++) {
                ArrayList<InventoryLogs.Data> filteredData = filterInventory(data, warehouses.get(i).warehouseNumber);
                fragments.add(new InventoryLogFragment(filteredData));
                tabLabels.add(warehouses.get(i).warehouseName);
            }
            pageAdapter = new PageAdapter(this, fragments);
            tabAdapter = new TabAdapter(tabLabels, 0);

            viewPager2.setAdapter(pageAdapter);
            categories.setAdapter(tabAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            categories.setLayoutManager(linearLayoutManager);

            tabAdapter.onClickCallback = new TabAdapter.OnClickCallback() {
                @Override
                public void onClicked(View v, int position) {
                    viewPager2.setCurrentItem(position, true);
                }
            };
            viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    categories.smoothScrollToPosition(position);
                    tabAdapter.setActiveId(position);
                }
            });
            categories.setVisibility(View.VISIBLE);
            switchLayoutState(STATE_CONTENT);
        }
    }

    protected ArrayList<InventoryLogs.Data> filterInventory(ArrayList<InventoryLogs.Data> data, int warehouseId) {
        ArrayList<InventoryLogs.Data> filteredData = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).warehouseId == warehouseId) {
                filteredData.add(data.get(i));
            }
        }
        return filteredData;
    }

}