package prokoding.gudangkita.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import io.github.muddz.styleabletoast.StyleableToast;
import prokoding.gudangkita.R;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;

public class LoginActivity extends AppCompatActivity {
    private MaterialButton btnLogin;
    private TextView txViewVersion;
    private AuthUtils.AuthLauncher authLauncher;
    private LinearProgressIndicator progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        handleMapComponents();
        handleRenderVersion();
        handleLogin();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(LoginActivity.this,AuthUtils.getCurrentUser(LoginActivity.this).mailAccount,"", "login_view");

    }

    @Override
    protected void onStart() {
        super.onStart();
        handleRedirect();
    }

    void handleMapComponents() {
        txViewVersion = findViewById(R.id.login_version);
        btnLogin = findViewById(R.id.login_button);
        progress = findViewById(R.id.login_progress);
        authLauncher = new AuthUtils.AuthLauncher(this);
        authLauncher.setCallback(new AuthUtils.Callback() {
            @Override
            public void onAuthStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess() {
                progress.setVisibility(View.INVISIBLE);
                String messageTemplate = getString(R.string.hint_account_registered);
                String firstName = AuthUtils.getCurrentFirebaseUser().getDisplayName().split("[\\s]+")[0];
                String message = String.format(messageTemplate, firstName);
                StyleableToast.makeText(LoginActivity.this, message, R.style.Theme_GudangKita_Toast_Success_Hello)
                        .show();
                handleRedirect();
            }

            @Override
            public void onFailed() {
                progress.setVisibility(View.INVISIBLE);
                btnLogin.setEnabled(true);
                StyleableToast.makeText(LoginActivity.this, getString(R.string.hint_account_not_registered), R.style.Theme_GudangKita_Toast_Danger_AccountRejected)
                        .show();
            }

            @Override
            public void onError() {
                progress.setVisibility(View.INVISIBLE);
                btnLogin.setEnabled(true);
                StyleableToast.makeText(LoginActivity.this, getString(R.string.hint_account_failed), R.style.Theme_GudangKita_Toast_Danger)
                        .show();
            }

            @Override
            public void onCancel() {
                btnLogin.setEnabled(true);
            }
        });
    }

    void handleRenderVersion() {
        try {
            String versionName = LoginActivity.this.getPackageManager().getPackageInfo(LoginActivity.this.getPackageName(), 0).versionName;
            String versionString = "v" + versionName;
            txViewVersion.setText(versionString);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    void handleLogin() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLogin.setEnabled(false);
                authLauncher.launch(LoginActivity.this);
            }
        });
    }

    void handleRedirect() {
        if (AuthUtils.isAuthValid(this)) {
            Intent intent = new Intent(this, StockoutActivity.class);
            startActivity(intent);
            finish();
        }
    }

}