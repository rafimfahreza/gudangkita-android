package prokoding.gudangkita.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.HorizontalScrollView;

import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.color.MaterialColors;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.adapters.PendingPageAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.fragments.PendingFragment;
import prokoding.gudangkita.models.Pending;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingListActivity extends SubAPIActivity {
    private PendingPageAdapter pageAdapter;
    private ArrayList<PendingFragment> fragments = new ArrayList<>();
    private ArrayList<Pending.Data> pendingData = new ArrayList<>();
    private MaterialButton btnStockout;
    private MaterialButton btnStockin;
    private MaterialButton btnCustomer;
    private HorizontalScrollView navigation;
    private ViewPager2 pager;
    private boolean isAdmin;
    private boolean isAccounting;
    private int currentPosition;
    PendingFragment.Callback pendingFragmentCallback = new PendingFragment.Callback() {
        @Override
        public void onBack() {
            handleFillData();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.pendinglist_title));
        handleMapComponents();
        handleFillData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(PendingListActivity.this,AuthUtils.getCurrentUser(PendingListActivity.this).mailAccount,"", "pending_list_view");
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_CARD, null);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_pending_list, null);
    }

    @Override
    protected void handleReload() {
        handleFillData();
    }

    protected void handleMapComponents() {
        isAccounting = AuthUtils.getCurrentUser(this).accountType >= 2;
        isAdmin = AuthUtils.getCurrentUser(this).accountType >= 3;
        btnStockout = findViewById(R.id.pending_button_stockout);
        btnStockin = findViewById(R.id.pending_button_stockin);
        btnCustomer = findViewById(R.id.pending_button_customers);
        navigation = findViewById(R.id.pending_navigation);
        pager = findViewById(R.id.pending_pager);
        if (!isAccounting) {
            finish();
            return;
        }
        if (!isAdmin) {
            btnCustomer.setVisibility(View.GONE);
        }
    }

    protected void handleFillData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<Pending> call = ApiRetrofit.getApi(this).getPendingList(appVersion, words, token);
        call.enqueue(new Callback<Pending>() {
            @Override
            public void onResponse(Call<Pending> call, Response<Pending> response) {
                if (response.body().data != null) {
                    handleOnResponse(response);
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(PendingListActivity.this,AuthUtils.getCurrentUser(PendingListActivity.this).mailAccount,"", "pendlist_prefetch_failed");
                }
                setAppbarBorderVisibility(View.VISIBLE);
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Pending> call, Throwable t) {
                setAppbarBorderVisibility(View.VISIBLE);
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleOnResponse(Response<Pending> response) {
        switchLayoutState(STATE_CONTENT);
        setAppbarBorderVisibility(View.GONE);
        fragments = new ArrayList<>();
        fragments.add(PendingFragment.newInstance("out", pendingFragmentCallback));
        fragments.add(PendingFragment.newInstance("in", pendingFragmentCallback));
        pageAdapter = new PendingPageAdapter(this, fragments);
        pendingData = new ArrayList<>(response.body().data);
        if (isAdmin) {
            fragments.add(PendingFragment.newInstance("cust", pendingFragmentCallback));
        }
        for (int i = 0; i < pendingData.size(); i++) {
            Pending.Data data = pendingData.get(i);
            String refId = data.refId;
            if (refId.startsWith("SJ")) {
                fragments.get(0).addData(data);
            } else if (refId.startsWith("STKIN")) {
                fragments.get(1).addData(data);
            } else if (refId.startsWith("CTM") && isAdmin) {
                fragments.get(2).addData(data);
            }
        }
        pager.setAdapter(pageAdapter);
        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                currentPosition = position;
                if (position == 0) {
                    navigation.smoothScrollTo((int) btnStockout.getX() - (btnStockout.getWidth() / 2), 0);
                    setBtnActive(btnStockout);
                    setBtnInactive(btnStockin);
                    setBtnInactive(btnCustomer);
                } else if (position == 1) {
                    navigation.smoothScrollTo((int) btnStockin.getX() - (btnStockin.getWidth() / 2), 0);
                    setBtnInactive(btnStockout);
                    setBtnActive(btnStockin);
                    setBtnInactive(btnCustomer);
                } else {
                    navigation.smoothScrollTo((int) btnCustomer.getX() - (btnCustomer.getWidth() / 2), 0);
                    setBtnInactive(btnStockout);
                    setBtnInactive(btnStockin);
                    setBtnActive(btnCustomer);
                }
            }
        });
        handleBindButtonToPager(btnStockout, 0);
        handleBindButtonToPager(btnStockin, 1);
        handleBindButtonToPager(btnCustomer, 2);
        navigation.setVisibility(View.VISIBLE);
        pager.setCurrentItem(currentPosition, false);
    }

    protected void handleBindButtonToPager(MaterialButton button, int position) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = position;
                pager.setCurrentItem(position, true);
            }
        });
    }

    protected void setBtnActive(MaterialButton button) {
        button.setBackgroundColor(MaterialColors.getColor(this, R.attr.colorPrimary, getResources().getColor(R.color.red)));
        button.setTextColor(MaterialColors.getColor(this, R.attr.colorOnPrimary, Color.WHITE));
    }

    protected void setBtnInactive(MaterialButton button) {
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setTextAppearance(this, R.style.Theme_GudangKita_TextView_Body);
        button.setTextColor(MaterialColors.getColor(this, R.attr.colorPrimary, getResources().getColor(R.color.red)));
    }
}
