package prokoding.gudangkita.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.adapters.InventoryAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class    InventoryActivity extends SubAPIActivity {
    private Intent inventoryDetail;
    private Intent inventoryAdd;
    private RecyclerView inventoryList;

    private final ActivityResultLauncher<Intent> launcher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    handlePrefetchData();
                }
            }
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.inventories_title));
        handleMapComponents();
        handlePrefetchData();
        handleAdd();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(InventoryActivity.this,AuthUtils.getCurrentUser(InventoryActivity.this).mailAccount,"", "inventory_view");
    }

    @Override
    protected View createNodata() {
        return getLayoutInflater().inflate(R.layout.component_nodata_add, null);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_inventory, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_CARD, null);
    }

    @Override
    protected void handleReload() {
        handlePrefetchData();
    }

    protected void handleMapComponents() {
        inventoryAdd = new Intent(this, InventoryAddActivity.class);
        inventoryDetail = new Intent(this, InventoryDetailsActivity.class);
        inventoryList = findViewById(R.id.inventory_list);
    }

    protected void handlePrefetchData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<Inventory> call = ApiRetrofit.getApi(this).getAllProducts(appVersion, words, token);
        call.enqueue(new Callback<Inventory>() {
            @Override
            public void onResponse(Call<Inventory> call, Response<Inventory> response) {
                if (response.body().data != null) {
                    if (response.body().data.size() <= 0) {
                        switchLayoutState(STATE_NODATA);
                        return;
                    }
                    switchLayoutState(STATE_CONTENT);
                    InventoryAdapter adapter = new InventoryAdapter(response.body().data);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(InventoryActivity.this);
                    adapter.onClickCallback = new InventoryAdapter.OnClickCallback() {
                        @Override
                        public void onClicked(Inventory.Data data) {
                            inventoryDetail.putExtra("data", data);
                            launcher.launch(inventoryDetail);
                            overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                        }
                    };
                    inventoryList.setLayoutManager(layoutManager);
                    inventoryList.setAdapter(adapter);
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(InventoryActivity.this,AuthUtils.getCurrentUser(InventoryActivity.this).mailAccount,response.body().responseMessage, "inventory_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Inventory> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleAdd() {
        FloatingActionButton fab = findViewById(R.id.inventory_fab_add);
        FloatingActionButton fab2 = findViewById(R.id.nodata_fab_add);
        if (AuthUtils.getCurrentUser(this).accountType < 3) {
            fab.setVisibility(View.GONE);
            fab2.setVisibility(View.GONE);
            return;
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(InventoryActivity.this,AuthUtils.getCurrentUser(InventoryActivity.this).mailAccount,"", "inventory_add_clicked");
                launcher.launch(inventoryAdd);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(InventoryActivity.this,AuthUtils.getCurrentUser(InventoryActivity.this).mailAccount,"", "inventory_add_clicked");
                launcher.launch(inventoryAdd);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
            }
        });
    }

}