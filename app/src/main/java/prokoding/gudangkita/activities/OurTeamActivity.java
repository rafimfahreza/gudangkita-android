package prokoding.gudangkita.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.adapters.MemberAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.Member;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OurTeamActivity extends SubAPIActivity {
    private RecyclerView ourTeamList;
    ActivityResultLauncher<Intent> launcher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    handlePrefetchData();
                }
            }
    );
    //    private MaterialAlertDialogBuilder removeDialog;
    //    private RemovalDialog removalDialog;
//    private boolean isAdmin;
    private boolean isAccounting;
    private Intent memberAddIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.ourteam_title));
        handleMapComponents();
        handlePrefetchData();
        handleAdd();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(OurTeamActivity.this,AuthUtils.getCurrentUser(OurTeamActivity.this).mailAccount,"", "member_view");

    }

    @Override
    protected View createNodata() {
        return getLayoutInflater().inflate(R.layout.component_nodata_add, null);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_our_team, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_CARD, null);
    }

    @Override
    protected void handleReload() {
        handlePrefetchData();
    }

    protected void handleMapComponents() {
//        isAdmin = AuthUtils.getCurrentUser(OurTeamActivity.this).accountType >= 3;
        isAccounting = AuthUtils.getCurrentUser(this).accountType >= 2;
        ourTeamList = findViewById(R.id.our_team_list);
        if (!isAccounting) {
            finish();
            return;
        }
        memberAddIntent = new Intent(OurTeamActivity.this, MemberAddActivity.class);
    }

    protected void handlePrefetchData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<Member> call = ApiRetrofit.getApi(this).getMembers(appVersion, words, token);
        call.enqueue(new Callback<Member>() {
            @Override
            public void onResponse(Call<Member> call, Response<Member> response) {
                if (response.body().data != null) {
                    switchLayoutState(STATE_CONTENT);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(OurTeamActivity.this);
                    MemberAdapter adapter = new MemberAdapter(response.body().data, false);
//                        if (isAdmin) {
//                            adapter.onRemoveCallback = new MemberAdapter.OnRemoveCallback() {
//                                @Override
//                                public void onClicked(Member.Data data) {
//                                    removalDialog = new RemovalDialog(OurTeamActivity.this, String.format(getString(R.string.remove_member_message), data.memberEmail), data.memberEmail);
//
//                                    removalDialog.show();
//                                }
//                            };
//                        }
                    ourTeamList.setLayoutManager(layoutManager);
                    ourTeamList.setAdapter(adapter);
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(OurTeamActivity.this,AuthUtils.getCurrentUser(OurTeamActivity.this).mailAccount,response.body().responseMessage, "member_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Member> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleAdd() {
        FloatingActionButton fab = findViewById(R.id.inventory_fab_add);
        FloatingActionButton fab2 = findViewById(R.id.nodata_fab_add);
        if (AuthUtils.getCurrentUser(this).accountType < 3) {
            fab.setVisibility(View.GONE);
            fab2.setVisibility(View.GONE);
            return;
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(OurTeamActivity.this,AuthUtils.getCurrentUser(OurTeamActivity.this).mailAccount,"", "add_member_clicked");
                launcher.launch(memberAddIntent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(OurTeamActivity.this,AuthUtils.getCurrentUser(OurTeamActivity.this).mailAccount,"", "add_member_clicked");
                launcher.launch(memberAddIntent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
            }
        });
    }

}