package prokoding.gudangkita.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.APIActivity;
import prokoding.gudangkita.adapters.NotificationAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.Notification;
import prokoding.gudangkita.utils.AuthUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends APIActivity {
    private RecyclerView recycler;
    private MaterialToolbar toolbar;
    private boolean isAccounting;
    private final ActivityResultLauncher<Intent> launcher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    handlePrefetchData();
                }
            }
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleMapComponents();
        handleNavigation();
        handlePrefetchData();
    }

    @NonNull
    @Override
    protected int getMainLayout() {
        return R.layout.activity_notification;
    }

    @Override
    protected ViewGroup createContainer() {
        return findViewById(R.id.notification_container);
    }

    @Override
    protected View createContent() {
        return findViewById(R.id.notification_recycler);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_CARD, null);
    }

    @Override
    protected View createNodata() {
        return getLayoutInflater().inflate(R.layout.component_notification_nodata, null);
    }

    private void handleMapComponents() {
        recycler = (RecyclerView) createContent();
        isAccounting = AuthUtils.getCurrentUser(this).accountType >= 2;
        toolbar = findViewById(R.id.notification_toolbar);
        if (!isAccounting) {
            finish();
        }
    }

    private void handlePrefetchData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        NotificationAdapter adapter = new NotificationAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        Call<Notification> call = ApiRetrofit.getApi(this).getNotifications(appVersion, words, token);
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                if (response.body().data != null) {
                    if (response.body().data != null) {
                        ArrayList<Notification.Data> data = response.body().data;
                        if (data.size() > 0) {
                            switchLayoutState(STATE_CONTENT);
                            for (int i = 0; i < data.size(); i++) {
                                String title = data.get(i).refId;
                                int state = NotificationAdapter.STATE_NEUTRAL;
                                adapter.addItem(title, null, state);
                            }
                            return;
                        }
                        switchLayoutState(STATE_NODATA);
                        return;
                    }
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    private void handleNavigation() {
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.notification_close) {
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.right_slide_in_reverse);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_slide_out_reverse, R.anim.right_slide_in_reverse);
    }
}
