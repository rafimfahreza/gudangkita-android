package prokoding.gudangkita.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.adapters.SjHistoryAdapter;
import prokoding.gudangkita.adapters.StockoutPictureAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.StockinHistory;
import prokoding.gudangkita.models.StockoutHistory;
import prokoding.gudangkita.models.StockoutHistoryDetail;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockoutDetailActivity extends SubAPIActivity {
    public static String ARG_REFID = "REF_ID";
    private AutoCompleteTextView id;
    private AutoCompleteTextView receiver;
    private AutoCompleteTextView emailMember;
    private TextInputEditText orderDate;
    private TextInputEditText deadlineDate;
    private AutoCompleteTextView deliveryDate;
    private LinearLayout deadlineWrapper;
    private MaterialToolbar toolbar;
    private String refId;
    private StockoutHistoryDetail.Data data;
    private RecyclerView products;
    private RecyclerView pictureStockout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.stockout_title));
        handleMapComponents();
        handleFillData();
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_stockout_detail, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(R.layout.component_item_field_shiver, null);
    }

    @Override
    protected void handleReload() {
        handleFillData();
    }

    protected void handleMapComponents() {
        id = findViewById(R.id.stockout_detail_id);
        receiver = findViewById(R.id.stockout_detail_receiver);
        emailMember = findViewById(R.id.stockout_detail_member);
        orderDate = findViewById(R.id.stockout_detail_date);
        deadlineDate = findViewById(R.id.stockout_detail_deadline_date);
        deadlineWrapper = findViewById(R.id.stockout_detail_deadline_wrapper);
        products = findViewById(R.id.stockout_detail_products);
        toolbar = findViewById(R.id.nonstockout_toolbar);
        deliveryDate = findViewById(R.id.stockout_delivery_date);
        pictureStockout = findViewById(R.id.pic_products);
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(StockoutDetailActivity.this,AuthUtils.getCurrentUser(StockoutDetailActivity.this).mailAccount,"", "stockout_detail_view");

    }

    protected void handleFillData() {
        switchLayoutState(STATE_SHIVER);
        toolbar.getMenu().clear();
        refId = getIntent().getStringExtra(ARG_REFID);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<StockoutHistoryDetail> call = ApiRetrofit.getApi(this).getStockoutHistoryDetails(appVersion, words, token, refId);
        call.enqueue(new Callback<StockoutHistoryDetail>() {
            @Override
            public void onResponse(Call<StockoutHistoryDetail> call, Response<StockoutHistoryDetail> response) {
                if (response.body().data != null) {
                    data = response.body().data;
                    switchLayoutState(STATE_CONTENT);
                    handleOnResponse();
                    handleNavigation();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockoutDetailActivity.this,AuthUtils.getCurrentUser(StockoutDetailActivity.this).mailAccount,response.body().responseMessage, "stockout_detail_failed");

                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<StockoutHistoryDetail> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleNavigation() {
        toolbar.inflateMenu(R.menu.stockout_detail_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.stockout_sj) {
                    Intent intent = new Intent(StockoutDetailActivity.this, SuratJalanImageActivity.class);
                    intent.putExtra(SuratJalanImageActivity.ARG_DATA, data.sjPicture);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
    }

    protected void handleOnResponse() {
        SjHistoryAdapter adapter = new SjHistoryAdapter(data.products);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        StockoutPictureAdapter stockoutPictureAdapter = new StockoutPictureAdapter(data.products);
        LinearLayoutManager stockoutPicLayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        pictureStockout.setLayoutManager(stockoutPicLayoutManager);
        pictureStockout.setAdapter(stockoutPictureAdapter);
        products.setLayoutManager(linearLayoutManager);
        products.setAdapter(adapter);
        id.setText(data.sjNumber);
        receiver.setText(data.receiver);
        emailMember.setText(data.emailMember);
        orderDate.setText(data.orderDate);
        deliveryDate.setText(data.deliveryDate);
        if (data.deadline != null) {
            deadlineDate.setText(data.deadline);
            deadlineWrapper.setVisibility(View.VISIBLE);
        } else {
            deadlineDate.setText("");
            deadlineWrapper.setVisibility(View.GONE);
        }
    }
}