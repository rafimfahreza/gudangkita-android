package prokoding.gudangkita.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.ArrayList;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.DocumentPicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.PendingStockin;
import prokoding.gudangkita.models.Warehouse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingStockinActivity extends SubAPIActivity {
    public static String ARG_REF_ID = "REF_ID";
    private final ArrayList<String> optProduct = new ArrayList<>();
    private final ArrayList<String> optWarehouseLocation = new ArrayList<>();
    private AutoCompleteTextView id;
    private AutoCompleteTextView product;
    private AutoCompleteTextView warehouseLocation;
    private AutoCompleteTextView poNumber;
    private TextInputEditText quantity;
    private MaterialButton submit;
    private MaterialButton reject;
    private DocumentPicker documentPicker;
    private ArrayList<Warehouse.Data> warehouseData;
    private ArrayList<Inventory.Data> inventoryData;
    private PendingStockin.Data detailData;
    private String refId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.pendinglist_title));
        handleMapComponents();
        handleBindEventValidate();
        handleFillData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(PendingStockinActivity.this,AuthUtils.getCurrentUser(PendingStockinActivity.this).mailAccount,"", "pend_stockin_view");

    }

    @Override
    protected void handleReload() {
        handleFillData();
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_pending_stockin, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    protected void handleMapComponents() {
        refId = getIntent().getStringExtra(ARG_REF_ID);
        id = findViewById(R.id.pending_stockin_id);
        product = findViewById(R.id.pending_stockin_product);
        warehouseLocation = findViewById(R.id.pending_stockin_warehouse_location);
        quantity = findViewById(R.id.pending_stockin_quantity);
        submit = findViewById(R.id.pending_stockin_submit);
        reject = findViewById(R.id.pending_stockin_reject);
        poNumber = findViewById(R.id.pending_stockin_ponumber);
        documentPicker = findViewById(R.id.pending_pending_stockin_upload_1);
    }

    protected void handleFillData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<Inventory> callInventory = ApiRetrofit.getApi(this).getAllProducts(appVersion, words, token);
        Call<Warehouse> callWarehouse = ApiRetrofit.getApi(this).getWarehouses(appVersion, words, token);
        Call<PendingStockin> callDetail = ApiRetrofit.getApi(this).getPendingStockin(appVersion, words, token, refId, "in");
        callInventory.enqueue(new Callback<Inventory>() {
            @Override
            public void onResponse(Call<Inventory> call, Response<Inventory> response) {
                if (response.body().data != null) {
                    inventoryData = response.body().data;
                    handleOnResponse();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(PendingStockinActivity.this,AuthUtils.getCurrentUser(PendingStockinActivity.this).mailAccount,response.body().responseMessage, "inv_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Inventory> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
        callWarehouse.enqueue(new Callback<Warehouse>() {
            @Override
            public void onResponse(Call<Warehouse> call, Response<Warehouse> response) {
                if (response.body().data != null) {
                    warehouseData = response.body().data;
                    handleOnResponse();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(PendingStockinActivity.this,AuthUtils.getCurrentUser(PendingStockinActivity.this).mailAccount,response.body().responseMessage, "wh_prefetch_failed");

                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Warehouse> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
        callDetail.enqueue(new Callback<PendingStockin>() {
            @Override
            public void onResponse(Call<PendingStockin> call, Response<PendingStockin> response) {
                if (response.body().data != null) {
                    detailData = response.body().data;
                    handleOnResponse();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(PendingStockinActivity.this,AuthUtils.getCurrentUser(PendingStockinActivity.this).mailAccount,response.body().responseMessage, "pend_stockin_failed");

                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<PendingStockin> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleOnResponse() {
        if (detailData != null && warehouseData != null && inventoryData != null) {
            switchLayoutState(STATE_CONTENT);

            optProduct.clear();
            optWarehouseLocation.clear();
            for (int i = 0; i < inventoryData.size(); i++) {
                optProduct.add(inventoryData.get(i).productName);
            }
            for (int i = 0; i < warehouseData.size(); i++) {
                optWarehouseLocation.add(warehouseData.get(i).warehouseName);
            }
            ArrayAdapter<String> productAdapter = new ArrayAdapter<>(PendingStockinActivity.this, R.layout.item_dropdown, optProduct);
            ArrayAdapter<String> warehouseLocationAdapter = new ArrayAdapter<>(PendingStockinActivity.this, R.layout.item_dropdown, optWarehouseLocation);

            product.setAdapter(productAdapter);
            warehouseLocation.setAdapter(warehouseLocationAdapter);

            id.setText(detailData.refId);
            product.setText(detailData.productaName, false);
            quantity.setText(String.valueOf(detailData.quantity));
            warehouseLocation.setText(detailData.warehouseName, false);
        }
    }

    protected void handleBindEventValidate() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("TAG", "CLICKED");
                handleValidate();
            }
        };
        DocumentPicker.OnFilePicked onFilePicked = new DocumentPicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filepath) {
                handleValidate();
            }
        };
        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit(true);
            }
        };
        View.OnClickListener onReject = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit(false);
            }
        };
        product.setOnItemClickListener(onDropdownChange);
        warehouseLocation.setOnItemClickListener(onDropdownChange);
        quantity.addTextChangedListener(onTextChange);
        poNumber.addTextChangedListener(onTextChange);
        submit.setOnClickListener(onSubmit);
        reject.setOnClickListener(onReject);
        documentPicker.setOnFilePicked(onFilePicked);
    }

    protected void handleSubmit(boolean isApproved) {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(PendingStockinActivity.this,AuthUtils.getCurrentUser(PendingStockinActivity.this).mailAccount,"", "pend_stockin_clicked");
        try {
            MultipartBody.Part filePart = null;
            String appVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            String words = AuthUtils.getWords(PendingStockinActivity.this);
            String token = AuthUtils.getToken(PendingStockinActivity.this);
            if (documentPicker.getFilePath() != null) {
                File file = new File(documentPicker.getFilePath());
                filePart = MultipartBody.Part.createFormData("refPicture", file.getName(),
                        RequestBody.create(
                                MediaType.parse(getContentResolver().getType(documentPicker.getFileUri())),
                                file
                        )
                );
            }
            ProgressDialogUtils.show(PendingStockinActivity.this);
            Call<BaseResponse> callStockin = ApiRetrofit.getApi(PendingStockinActivity.this).updatePendingStockin(
                    MultipartBody.create(MultipartBody.FORM, appVersion),
                    MultipartBody.create(MultipartBody.FORM, words),
                    MultipartBody.create(MultipartBody.FORM, token),
                    MultipartBody.create(MultipartBody.FORM, refId),
                    MultipartBody.create(MultipartBody.FORM, String.valueOf(inventoryData.get(optProduct.indexOf(product.getText().toString())).productNumber)),
                    MultipartBody.create(MultipartBody.FORM, String.valueOf(warehouseData.get(optWarehouseLocation.indexOf(warehouseLocation.getText().toString())).warehouseNumber)),
                    MultipartBody.create(MultipartBody.FORM, quantity.getText().toString()),
                    MultipartBody.create(MultipartBody.FORM, poNumber.getText().toString()),
                    MultipartBody.create(MultipartBody.FORM, String.valueOf(isApproved)),
                    MultipartBody.create(MultipartBody.FORM, String.valueOf(AuthUtils.getCurrentUser(PendingStockinActivity.this).userId)),
                    filePart
            );
            callStockin.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    ProgressDialogUtils.dismiss();
                    if (response.body().responseMessage.equalsIgnoreCase("success")) {
                        String message = isApproved ? "Stock in successfully approved" : "Stock in successfully rejected";
                        StyleableToast.makeText(PendingStockinActivity.this, message, R.style.Theme_GudangKita_Toast_Success).show();
                        finish();
                        return;
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(PendingStockinActivity.this,AuthUtils.getCurrentUser(PendingStockinActivity.this).mailAccount,response.body().responseMessage, "pend_stockin_failed");

                    }
                    StyleableToast.makeText(PendingStockinActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    ProgressDialogUtils.dismiss();
                    StyleableToast.makeText(PendingStockinActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                }
            });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void handleValidate() {
        int quantityInt = 0;
        String quantityStr = quantity.getText().toString();
        if (!quantityStr.isEmpty()) {
            quantityInt = Integer.valueOf(quantityStr);
        }
        boolean valid = quantityInt > 0 &&
                optProduct.contains(product.getText().toString()) &&
                !poNumber.getText().toString().isEmpty() &&
                documentPicker.getFilePath() != null &&
                optWarehouseLocation.contains(warehouseLocation.getText().toString());
        submit.setEnabled(valid);
        reject.setEnabled(valid);
    }
}