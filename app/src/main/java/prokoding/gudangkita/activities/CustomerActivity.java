package prokoding.gudangkita.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.RajaOngkirProvince;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends SubAPIActivity {
    TextInputEditText name;
    TextInputEditText phoneNumber;
    AutoCompleteTextView idType;
    TextInputEditText idNumber;
    TextInputEditText address;
    AutoCompleteTextView province;
    AutoCompleteTextView type;
    ImagePicker imagePicker1;
    MaterialButton submit;
    ArrayList<String> optIdType = new ArrayList<>();
    ArrayList<String> optProvince = new ArrayList<>();
    ArrayList<String> optType = new ArrayList<>();
    TextInputLayout idTypeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.customers_title));
        handleMapComponents();
        handlePrefetchData();
        handleBindEvents();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(CustomerActivity.this,AuthUtils.getCurrentUser(CustomerActivity.this).mailAccount,"", "customer_view");

    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_customer, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    protected void handleMapComponents() {
        name = findViewById(R.id.customers_name);
        phoneNumber = findViewById(R.id.customers_phone);
        idType = findViewById(R.id.customers_id_type);
        idTypeContainer = findViewById(R.id.customers_id_type_container);
        idNumber = findViewById(R.id.customers_id_number);
        address = findViewById(R.id.customers_address);
        province = findViewById(R.id.customers_province);
        type = findViewById(R.id.customers_type);
        imagePicker1 = findViewById(R.id.customers_upload_1);
        submit = findViewById(R.id.customers_submit);
    }

    @Override
    protected void handleReload() {
        handlePrefetchData();
    }

    protected void handlePrefetchData() {
        switchLayoutState(STATE_SHIVER);
        optIdType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_id_type)));
        optType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_customer_type)));

        ArrayAdapter<String> idTypeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optIdType);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optType);

        idType.setAdapter(idTypeAdapter);
        type.setAdapter(typeAdapter);

        Call<RajaOngkirProvince> call = ApiRetrofit.getRajaOngkirApi(this).getProvinces(getString(R.string.config_api_rajaoingkir_key));
        call.enqueue(new Callback<RajaOngkirProvince>() {
            @Override
            public void onResponse(Call<RajaOngkirProvince> call, Response<RajaOngkirProvince> response) {
                if (response.body().rajaongkir != null) {
                    if (response.body().rajaongkir.results != null) {
                        ArrayList<RajaOngkirProvince.Rajaongkir.Data> provinces = response.body().rajaongkir.results;
                        if (provinces.size() > 0) {
                            for (int i = 0; i < provinces.size(); i++) {
                                optProvince.add(provinces.get(i).province);
                            }
                            ArrayAdapter<String> provinceAdapter = new ArrayAdapter<>(CustomerActivity.this, R.layout.item_dropdown, optProvince);
                            province.setAdapter(provinceAdapter);
                            switchLayoutState(STATE_CONTENT);
                            return;
                        }
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(CustomerActivity.this,AuthUtils.getCurrentUser(CustomerActivity.this).mailAccount,"", "rajaongkir_api_failed");
                    }
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(CustomerActivity.this,AuthUtils.getCurrentUser(CustomerActivity.this).mailAccount,"", "rajaongkir_api_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<RajaOngkirProvince> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleBindEvents() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleValidate();
            }
        };

        AdapterView.OnItemClickListener onTypeChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (optType.get(position).equalsIgnoreCase("corporate")) {
                    idType.setText("NPWP", false);
                    idTypeContainer.setEnabled(false);
                    idType.setEnabled(false);

                } else if (!idType.getText().toString().isEmpty()) {
                    idType.setText("KTP", false);
                    idTypeContainer.setEnabled(true);
                    idType.setEnabled(true);
                }
                handleValidate();
            }
        };

        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };

        ImagePicker.OnFilePicked onFilePicked = new ImagePicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filepath) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        };
        type.setOnItemClickListener(onTypeChange);
        name.addTextChangedListener(onTextChange);
        phoneNumber.addTextChangedListener(onTextChange);
        idType.setOnItemClickListener(onDropdownChange);
        idNumber.addTextChangedListener(onTextChange);
        address.addTextChangedListener(onTextChange);
        province.setOnItemClickListener(onDropdownChange);
        imagePicker1.setOnFilePicked(onFilePicked);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleSubmit() {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(CustomerActivity.this,AuthUtils.getCurrentUser(CustomerActivity.this).mailAccount,"", "add_customer_clicked");
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        File file = new File(imagePicker1.getFilePath());
        ProgressDialogUtils.show(this);
        Call<BaseResponse> call = ApiRetrofit.getApi(this).addCustomer(
                RequestBody.create(MultipartBody.FORM, appVersion),
                RequestBody.create(MultipartBody.FORM, words),
                RequestBody.create(MultipartBody.FORM, token),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optType.indexOf(type.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, name.getText().toString()),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optIdType.indexOf(idType.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, idNumber.getText().toString()),
                RequestBody.create(MultipartBody.FORM, address.getText().toString()),
                RequestBody.create(MultipartBody.FORM, province.getText().toString()),
                RequestBody.create(MultipartBody.FORM, phoneNumber.getText().toString()),
                RequestBody.create(MultipartBody.FORM, String.valueOf(AuthUtils.getCurrentUser(CustomerActivity.this).userId)),
                MultipartBody.Part.createFormData("identityPicture", file.getName(),
                        RequestBody.create(
                                MediaType.parse(getContentResolver().getType(imagePicker1.getFileUri())),
                                file
                        )
                )
        );
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().responseMessage != null) {
                    if (response.body().responseMessage.equalsIgnoreCase("success")) {
                        handleClear();
                        StyleableToast.makeText(CustomerActivity.this, "Customer successfully addded", R.style.Theme_GudangKita_Toast_Success).show();
                        return;
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(CustomerActivity.this,AuthUtils.getCurrentUser(CustomerActivity.this).mailAccount,response.body().responseMessage, "add_customer_failed");
                    }
                }
                StyleableToast.makeText(CustomerActivity.this, getString(R.string.hint_request_failed), R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                StyleableToast.makeText(CustomerActivity.this, getString(R.string.hint_request_failed), R.style.Theme_GudangKita_Toast_Danger).show();
                ProgressDialogUtils.dismiss();
                t.printStackTrace();
            }
        });
    }

    protected void handleClear() {
        type.setText("", false);
        name.setText("");
        phoneNumber.setText("");
        idType.setText("", false);
        idNumber.setText("");
        address.setText("");
        province.setText("", false);
        imagePicker1.removeImage();
        handleValidate();
    }

    protected void handleValidate() {
        boolean valid = !name.getText().toString().isEmpty() &&
                !phoneNumber.getText().toString().isEmpty() &&
                optIdType.contains(idType.getText().toString()) &&
                !idNumber.getText().toString().isEmpty() &&
                !address.getText().toString().isEmpty() &&
                optProvince.contains(province.getText().toString()) &&
                optType.contains(type.getText().toString()) &&
                imagePicker1.getFilePath() != null;
        submit.setEnabled(valid);
    }

}