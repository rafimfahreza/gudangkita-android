package prokoding.gudangkita.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.color.MaterialColors;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.APIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.CameraLauncher;
import prokoding.gudangkita.models.Customer;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.LastSJNumber;
import prokoding.gudangkita.models.Stockout;
import prokoding.gudangkita.models.Warehouse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StockoutActivity extends APIActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    private LinearLayout containerQuantity;
    private LinearLayout wrapperType;

    private TextInputLayout quantityItemContainer;
    private TextInputLayout warehouseContainer;
    private TextInputLayout typeContainer;
    private TextInputLayout customerContainer;
    private TextInputLayout paymentContainer;
    private TextInputLayout deliveryContainer;

    private AutoCompleteTextView quantityItem;
    private AutoCompleteTextView quantity;
    private AutoCompleteTextView dropdownType;
    private AutoCompleteTextView dropdownCustomerName;
    private AutoCompleteTextView dropdownProduct;
    private AutoCompleteTextView dropdownPaymentType;
    private AutoCompleteTextView dropdownWarehouseLocation;
    private AutoCompleteTextView dropdownDeliveryMethod;
    private AutoCompleteTextView deliveryDate;
    private MaterialDatePicker datePicker;
    private MaterialPickerOnPositiveButtonClickListener datePickerPositiveListener;

    private Stockout.Data headerData = null;

    private ArrayList<String> optType = new ArrayList<>();
    private final ArrayList<String> optCustomer = new ArrayList<>();
    private final ArrayList<String> optProduct = new ArrayList<>();
    private final ArrayList<String> optProductExclude = new ArrayList<>();
    private ArrayList<String> optPaymentType = new ArrayList<>();
    private final ArrayList<String> optWarehouse = new ArrayList<>();
    private ArrayList<String> optDeliveryMethod = new ArrayList<>();
    private ArrayList<Inventory.Data> inventoryData = new ArrayList<>();
    private ArrayList<Customer.Data> customerData = new ArrayList<>();
    private ArrayList<Warehouse.Data> warehouseData = new ArrayList<>();
    private CameraLauncher cameraLauncher;
    private MaterialButton submit;

    private int maxItemQuantity;
    private int position = 1;
    private int count = 1;
    private String orderId;
    protected ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    handleClearAll();
                }
            }
    );
    private boolean isAccounting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleMapComponents();
        handleFillData();
        handleBindEvent();
        handleNavigation();
        handleNavigationHeader();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(this).mailAccount,"", "stockout_view");
    }

    @NonNull
    @Override
    protected int getMainLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected ViewGroup createContainer() {
        return findViewById(R.id.stockout_container);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_stockout, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    @Override
    protected View createNodata() {
        return getLayoutInflater().inflate(R.layout.component_nodata_stockout, null);
    }

    @Override
    protected void handleReload() {
        handleClearAll();
    }

    protected void handleMapComponents() {
        navigationView = findViewById(R.id.stockout_navigation);
        drawerLayout = findViewById(R.id.stockout_drawer);
        toolbar = findViewById(R.id.stockout_toolbar);
        containerQuantity = findViewById(R.id.stockout_container_quantity_of_items);
        wrapperType = findViewById(R.id.stockout_type_wrapper);
        quantityItem = findViewById(R.id.stockout_quantity_of_items);
        quantity = findViewById(R.id.stockout_quantity);
        deliveryDate = findViewById(R.id.delivery_date);

        typeContainer = findViewById(R.id.stockout_type_container);
        quantityItemContainer = findViewById(R.id.stockout_quantity_of_items_container);
        customerContainer = findViewById(R.id.stockout_customer_name_container);
        deliveryContainer = findViewById(R.id.stockout_delivery_method_container);
        paymentContainer = findViewById(R.id.stockout_payment_type_container);
        warehouseContainer = findViewById(R.id.stockout_warehouse_location_container);

        dropdownType = findViewById(R.id.stockout_type);
        dropdownCustomerName = findViewById(R.id.stockout_customer_name);
        dropdownProduct = findViewById(R.id.stockout_product);
        dropdownPaymentType = findViewById(R.id.stockout_payment_type);
        dropdownWarehouseLocation = findViewById(R.id.stockout_warehouse_location);
        dropdownDeliveryMethod = findViewById(R.id.stockout_delivery_method);
        cameraLauncher = findViewById(R.id.stockout_upload_1);
        submit = findViewById(R.id.stockout_submit);
        datePicker = MaterialDatePicker.Builder.datePicker().build();
        deliveryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    handleDeliveryDate();
                }
            }
        });
        deliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDeliveryDate();
            }
        });
        isAccounting = AuthUtils.getCurrentUser(this).accountType >= 2;
        if (!isAccounting) {
            navigationView.getMenu().findItem(R.id.navigation_pendinglist).setVisible(false);
            navigationView.getMenu().findItem(R.id.navigation_ourteam).setVisible(false);
            navigationView.getMenu().findItem(R.id.navigation_history).setVisible(false);
            toolbar.getMenu().clear();
        }
    }

    protected void handleDeliveryDate() {
        try {
            datePicker.removeOnPositiveButtonClickListener(datePickerPositiveListener);
        } finally {
            datePickerPositiveListener = new MaterialPickerOnPositiveButtonClickListener() {
                @Override
                public void onPositiveButtonClick(Object selection) {
                    long timestamp = Long.parseLong(selection.toString());
                    Date newDate = new Date(timestamp);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    deliveryDate.setText(sdf.format(newDate));
                }
            };
            datePicker.addOnPositiveButtonClickListener(datePickerPositiveListener);
            datePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
        }
    }

    protected void handleFillData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<Customer> callCustomer = ApiRetrofit.getApi(this).getCustomers(appVersion, words, token);
        Call<Inventory> callInventory = ApiRetrofit.getApi(this).getProducts(appVersion, words, token);
        Call<Warehouse> callWarehouse = ApiRetrofit.getApi(this).getWarehouses(appVersion, words, token);
        Call<LastSJNumber> callSj = ApiRetrofit.getApi(this).getLastSjNumber(appVersion, words, token);
        callCustomer.enqueue(new Callback<Customer>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                if (response.body().data != null) {
                    customerData = response.body().data;
                    handleResponseFillDataApi();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(StockoutActivity.this).mailAccount,response.body().responseMessage, "stockout_cst_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
            }
        });
        callInventory.enqueue(new Callback<Inventory>() {
            @Override
            public void onResponse(Call<Inventory> call, Response<Inventory> response) {
                if (response.body().data != null) {
                    inventoryData = response.body().data;
                    handleResponseFillDataApi();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(StockoutActivity.this).mailAccount,response.body().responseMessage, "stockout_inv_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Inventory> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
            }
        });
        callWarehouse.enqueue(new Callback<Warehouse>() {
            @Override
            public void onResponse(Call<Warehouse> call, Response<Warehouse> response) {
                if (response.body().data != null) {
                    warehouseData = response.body().data;
                    handleResponseFillDataApi();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(StockoutActivity.this).mailAccount,response.body().responseMessage, "stockout_wh_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Warehouse> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
            }
        });
        callSj.enqueue(new Callback<LastSJNumber>() {
            @Override
            public void onResponse(Call<LastSJNumber> call, Response<LastSJNumber> response) {
                if (response.body().data != null) {
                    orderId = response.body().data.lastSjNumber;
                    handleResponseFillDataApi();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(StockoutActivity.this).mailAccount,response.body().responseMessage, "stockout_sj_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<LastSJNumber> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
            }
        });
    }

    protected void handleResponseFillDataApi() {
        if (customerData != null && inventoryData != null && warehouseData != null && customerData != null && orderId != null) {
            optCustomer.clear();
            optProduct.clear();
            optProductExclude.clear();
            optWarehouse.clear();
            for (int i = 0; i < customerData.size(); i++) {
                optCustomer.add(customerData.get(i).customerName);
            }
            for (int i = 0; i < inventoryData.size(); i++) {
                optProduct.add(inventoryData.get(i).productName);
            }
            for (int i = 0; i < warehouseData.size(); i++) {
                optWarehouse.add(warehouseData.get(i).warehouseName);
            }

            if (customerData.size() > 0 && inventoryData.size() > 0 && warehouseData.size() > 0) {
                optType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_stockout_type)));
                optDeliveryMethod = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_delivery_method)));
                optPaymentType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_payment)));
                maxItemQuantity = inventoryData.size();

                if (optProduct.size() > 1) {
                    wrapperType.setVisibility(View.VISIBLE);
                    dropdownType.setText("", false);
                } else {
                    wrapperType.setVisibility(View.GONE);
                    dropdownType.setText(optType.get(0));
                }

                ArrayAdapter<String> adapterType = new ArrayAdapter(this, R.layout.item_dropdown, optType);
                ArrayAdapter<String> adapterDeliveryMethod = new ArrayAdapter<>(this, R.layout.item_dropdown, optDeliveryMethod);
                ArrayAdapter<String> adapterPaymentType = new ArrayAdapter<>(this, R.layout.item_dropdown, optPaymentType);
                ArrayAdapter<String> adapterCustomer = new ArrayAdapter<>(this, R.layout.item_dropdown, optCustomer);
                ArrayAdapter<String> adapterWarehouse = new ArrayAdapter<>(this, R.layout.item_dropdown, optWarehouse);
                ArrayAdapter<String> adapterProduct = new ArrayAdapter<String>(this, R.layout.item_dropdown, optProduct) {
                    @Override
                    public boolean isEnabled(int position) {
                        return !optProductExclude.contains(this.getItem(position));
                    }

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        TextView view = (TextView)super.getView(position, convertView, parent);
                        if (optProductExclude.contains(this.getItem(position))) {
                            view.setAlpha(0.5f);
                        } else {
                            view.setAlpha(1);
                        }
                        return view;
                    }
                };

                dropdownCustomerName.clearListSelection();
                dropdownCustomerName.setAdapter(adapterCustomer);
                dropdownProduct.clearListSelection();
                dropdownProduct.setAdapter(adapterProduct);
                dropdownWarehouseLocation.clearListSelection();
                dropdownWarehouseLocation.setAdapter(adapterWarehouse);
                dropdownDeliveryMethod.clearListSelection();
                dropdownDeliveryMethod.setAdapter(adapterDeliveryMethod);
                dropdownPaymentType.clearListSelection();
                dropdownPaymentType.setAdapter(adapterPaymentType);
                dropdownType.clearListSelection();
                dropdownType.setAdapter(adapterType);
                switchLayoutState(STATE_CONTENT);
                return;
            }
            switchLayoutState(STATE_NODATA);

        }
    }

    protected void handleNavigation() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.appbar_notifications) {
                    Intent intent = new Intent(StockoutActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.fade_out);
                    return true;
                }
                return false;
            }
        });
        navigationView.setNavigationItemSelectedListener(this);
    }

    protected void handleNavigationHeader() {
        View header = navigationView.getHeaderView(0);
        ImageView headerImage = header.findViewById(R.id.header_image);
        TextView headerText = header.findViewById(R.id.header_text);
        TextView headerSubText = header.findViewById(R.id.header_subtext);
        FloatingActionButton headerSignout = header.findViewById(R.id.header_signout);
        Glide.with(this)
                .load(AuthUtils.getCurrentFirebaseUser().getPhotoUrl())
                .circleCrop()
                .into(headerImage);
        headerText.setText(AuthUtils.getCurrentFirebaseUser().getDisplayName());
        headerSubText.setText(getResources().getStringArray(R.array.value_account_type)[AuthUtils.getCurrentUser(this).accountType - 1]);
        headerSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StockoutActivity.this, LoginActivity.class);
                AuthUtils.clearCurrentUser(StockoutActivity.this);
                startActivity(intent);
                finish();
            }
        });
    }

    protected void handleSubmit() {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(StockoutActivity.this).mailAccount,"", "stockout_submit_clicked");
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        File file = new File(cameraLauncher.getFilePath());
        ProgressDialogUtils.show(this);
        Call<Stockout> call = ApiRetrofit.getApi(this).addStockout(
                // its better to save appVersion, words, token to request header instead of its body
                // since we can set it in retrofit interface so u don't have to put it numerous of time
                // unfortunately the server require that in the request body, so happy spaghetti
                RequestBody.create(MultipartBody.FORM, appVersion),
                RequestBody.create(MultipartBody.FORM, words),
                RequestBody.create(MultipartBody.FORM, token),
                RequestBody.create(MultipartBody.FORM, orderId),

                // type, payment, quantity
                RequestBody.create(MultipartBody.FORM, String.valueOf(optType.indexOf(dropdownType.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optPaymentType.indexOf(dropdownPaymentType.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, quantity.getText().toString()),

                // customer, redundant params just need id
                RequestBody.create(MultipartBody.FORM, String.valueOf(customerData.get(optCustomer.indexOf(dropdownCustomerName.getText().toString())).customerNumber)),
                RequestBody.create(MultipartBody.FORM, dropdownCustomerName.getText().toString()),

                // product, redundant params just need id
                RequestBody.create(MultipartBody.FORM, String.valueOf(inventoryData.get(optProduct.indexOf(dropdownProduct.getText().toString())).productNumber)),
                RequestBody.create(MultipartBody.FORM, dropdownProduct.getText().toString()),

                // warehouse, redundant params just need id
                RequestBody.create(MultipartBody.FORM, String.valueOf(warehouseData.get(optWarehouse.indexOf(dropdownWarehouseLocation.getText().toString())).warehouseNumber)),
                RequestBody.create(MultipartBody.FORM, dropdownWarehouseLocation.getText().toString()),

                RequestBody.create(MultipartBody.FORM, String.valueOf(AuthUtils.getCurrentUser(StockoutActivity.this).userId)),
                // deliveryDate
                RequestBody.create(MultipartBody.FORM, deliveryDate.getText().toString()),
                MultipartBody.Part.createFormData("refPic", file.getName(),
                        RequestBody.create(
                                MediaType.parse(cameraLauncher.getFilePath()),
                                file
                        )
                )
        );
        call.enqueue(new Callback<Stockout>() {
            @Override
            public void onResponse(Call<Stockout> call, Response<Stockout> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().data != null) {
                    headerData = response.body().data;
                    StyleableToast.makeText(StockoutActivity.this, "Item successfully added", R.style.Theme_GudangKita_Toast_Success).show();
                    if (position < count) {
                        optProductExclude.add(dropdownProduct.getText().toString());
                        position += 1;
                        handleClearProduct();
                    } else {
                        Intent intent = new Intent(StockoutActivity.this, SuratJalanActivity.class);
                        intent.putExtra(SuratJalanActivity.ARG_DATA, headerData);
                        intent.putExtra(SuratJalanActivity.ARG_SHIPPING, dropdownDeliveryMethod.getText().toString());
                        activityResultLauncher.launch(intent);
                    }
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockoutActivity.this,AuthUtils.getCurrentUser(StockoutActivity.this).mailAccount,response.body().responseMessage, "stockout_submit_failed");
                }
                StyleableToast.makeText(StockoutActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<Stockout> call, Throwable t) {
                ProgressDialogUtils.dismiss();
                Log.d("TAG", "Exception Error "+t.toString());
                StyleableToast.makeText(StockoutActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
            }
        });
    }

    protected void handleClearProduct() {
        dropdownType.setText(optType.get(optType.size() - 1));
        dropdownCustomerName.setText(headerData.customerName);

        dropdownType.setEnabled(false);
        dropdownCustomerName.setEnabled(false);
        quantityItem.setEnabled(false);
        dropdownDeliveryMethod.setEnabled(false);
        dropdownPaymentType.setEnabled(false);
        dropdownWarehouseLocation.setEnabled(false);

        typeContainer.setEnabled(false);
        customerContainer.setEnabled(false);
        quantityItemContainer.setEnabled(false);
        deliveryContainer.setEnabled(false);
        paymentContainer.setEnabled(false);
        warehouseContainer.setEnabled(false);

        quantity.setText("", false);
        quantity.setHint("Quantity");
        dropdownProduct.setText("", false);
        cameraLauncher.removeImage();
        submit.setEnabled(false);
    }

    protected void handleClearAll() {
        headerData = null;
        position = 1;
        count = 1;

        dropdownType.setEnabled(true);
        dropdownCustomerName.setEnabled(true);
        quantityItem.setEnabled(true);
        dropdownDeliveryMethod.setEnabled(true);
        dropdownPaymentType.setEnabled(true);
        dropdownWarehouseLocation.setEnabled(true);

        typeContainer.setEnabled(true);
        customerContainer.setEnabled(true);
        quantityItemContainer.setEnabled(true);
        deliveryContainer.setEnabled(true);
        paymentContainer.setEnabled(true);
        warehouseContainer.setEnabled(true);

        containerQuantity.setVisibility(View.GONE);
        quantityItem.setText("2");
        quantity.setText("", false);
        dropdownCustomerName.setText("", false);
        dropdownProduct.setText("", false);
        dropdownProduct.setText("", false);
        dropdownPaymentType.setText("", false);
        dropdownWarehouseLocation.setText("", false);
        dropdownDeliveryMethod.setText("", false);

        cameraLauncher.removeImage();
        submit.setEnabled(false);
        handleFillData();
    }

    protected void handleBindEvent() {
        AdapterView.OnItemClickListener onTypeChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    position = 1;
                    count = 2;
                    quantityItem.setText("2");
                    containerQuantity.setVisibility(View.VISIBLE);
                } else {
                    containerQuantity.setVisibility(View.GONE);
                }
                handleValidate();
            }
        };
        View.OnFocusChangeListener onQuantityItemFocus = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                boolean isMultiple;
                if (!hasFocus) {
                    boolean isQuantityItem = !quantityItem.getText().toString().isEmpty();
                    if (isQuantityItem) {
                        int intQuantityItem = Integer.parseInt(quantityItem.getText().toString());
                        isMultiple = intQuantityItem >= 2;
                    } else {
                        isMultiple = false;
                    }
                    if (!isMultiple) {
                        position = 1;
                        count = 2;
                        quantityItem.setText("2");
                    }
                }
            }
        };
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String quantityHint = "Quantity";
                try {
                    Inventory.Data inventory = inventoryData.get(optProduct.indexOf(dropdownProduct.getText().toString()));
                    int quantityMax = inventory.baseStock;
                    quantityHint = "Current stock: " + quantityMax;

                    int quantityNum = Integer.parseInt(quantity.getText().toString());
                    quantityNum = Math.min(quantityNum, quantityMax);
                    quantity.setText(String.valueOf(quantityNum));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                quantity.setHint(quantityHint);
                handleValidate();
            }
        };
        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    Inventory.Data inventory = inventoryData.get(optProduct.indexOf(dropdownProduct.getText().toString()));
                    int quantityNum = Integer.parseInt(quantity.getText().toString());
                    int quantityMax = inventory.baseStock;
                    quantityNum = Math.min(quantityNum, quantityMax);
                    quantity.removeTextChangedListener(this);
                    quantity.setText(String.valueOf(quantityNum));
                    quantity.setSelection(quantity.getText().toString().length());
                    quantity.addTextChangedListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    int typeIndex = optType.indexOf(dropdownType.getText().toString());
                    if (typeIndex == 1) {
                        int quantityItemNum = Integer.parseInt(quantityItem.getText().toString());
                        quantityItemNum = Math.max(Math.min(quantityItemNum, maxItemQuantity), 2);
                        quantityItem.removeTextChangedListener(this);
                        quantityItem.setText(String.valueOf(quantityItemNum), false);
                        count = quantityItemNum;
                        quantityItem.setSelection(quantityItem.getText().toString().length());
                        quantityItem.addTextChangedListener(this);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                handleValidate();
            }
        };
        CameraLauncher.OnCameraCaptured onCameraCaptured = new CameraLauncher.OnCameraCaptured() {
            @Override
            public void onCameraCaptured(String filepath) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        };
        dropdownType.setOnItemClickListener(onTypeChange);
        dropdownProduct.setOnItemClickListener(onDropdownChange);
        dropdownProduct.addTextChangedListener(onTextChange);
        dropdownCustomerName.setOnItemClickListener(onDropdownChange);
        dropdownCustomerName.addTextChangedListener(onTextChange);
        dropdownDeliveryMethod.setOnItemClickListener(onDropdownChange);
        dropdownWarehouseLocation.setOnItemClickListener(onDropdownChange);
        dropdownPaymentType.setOnItemClickListener(onDropdownChange);
        quantityItem.addTextChangedListener(onTextChange);
        quantityItem.setOnFocusChangeListener(onQuantityItemFocus);
        quantity.addTextChangedListener(onTextChange);
        cameraLauncher.setOnCameraCaptured(onCameraCaptured);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleValidate() {
        int typeIndex = optType.indexOf(dropdownType.getText().toString());
        boolean isQuantityItem = !quantityItem.getText().toString().isEmpty();
        boolean isQuantity = !quantity.getText().toString().isEmpty();
        boolean isDeliveryDate = !deliveryDate.getText().toString().isEmpty();
        boolean isValid = typeIndex >= 0 && isQuantity &&
                optProduct.contains(dropdownProduct.getText().toString()) &&
                optCustomer.contains(dropdownCustomerName.getText().toString()) &&
                optDeliveryMethod.contains(dropdownDeliveryMethod.getText().toString()) &&
                optPaymentType.contains(dropdownPaymentType.getText().toString()) &&
                optWarehouse.contains(dropdownWarehouseLocation.getText().toString()) &&
                !optProductExclude.contains(dropdownProduct.getText().toString()) &&
                cameraLauncher.getFilePath() != null && isDeliveryDate;
        if (isQuantity) {
            isValid = isValid && Integer.parseInt(quantity.getText().toString()) > 0;
        }
        if (typeIndex == 1) {
            isValid = isValid && isQuantityItem;
            if (isQuantityItem) {
                boolean isMultiple = Integer.parseInt(quantityItem.getText().toString()) > 1;
                isValid = isValid && isMultiple;
                if (isMultiple) {
                    count = Integer.parseInt(quantityItem.getText().toString());
                    String submitText = String.format("Submit (%d/%d)", position, count);
                    submit.setText(submitText);
                }
            }
        } else {
            position = 1;
            count = 1;
            submit.setText("Submit");
        }
        submit.setEnabled(isValid);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawerLayout = findViewById(R.id.stockout_drawer);
        switch (item.getItemId()) {
            case R.id.navigation_stockin: {
                Intent intent = new Intent(StockoutActivity.this, StockinActivity.class);
                activityResultLauncher.launch(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                break;
            }
            case R.id.navigation_inventories: {
                Intent intent = new Intent(StockoutActivity.this, InventoryActivity.class);
                activityResultLauncher.launch(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                break;
            }
            case R.id.navigation_customers: {
                Intent intent = new Intent(StockoutActivity.this, CustomerActivity.class);
                activityResultLauncher.launch(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                break;
            }
            case R.id.navigation_ourteam: {
                Intent intent = new Intent(StockoutActivity.this, OurTeamActivity.class);
                activityResultLauncher.launch(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                break;
            }
            case R.id.navigation_pendinglist: {
                Intent intent = new Intent(StockoutActivity.this, PendingListActivity.class);
                activityResultLauncher.launch(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                break;
            }

            case R.id.navigation_history: {
                Intent intent = new Intent(StockoutActivity.this, HistoryActivity.class);
                activityResultLauncher.launch(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                break;
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}