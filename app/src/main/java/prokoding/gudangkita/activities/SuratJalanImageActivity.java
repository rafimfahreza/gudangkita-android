package prokoding.gudangkita.activities;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;

public class SuratJalanImageActivity extends SubActivity {
    public static final String ARG_DATA = "DATA";
    private ImageView suratJalan;
    private MaterialToolbar toolbar;
    private String urlImage;
    private MaterialButton printButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Surat Jalan");
        handleMapComponents();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(SuratJalanImageActivity.this,AuthUtils.getCurrentUser(SuratJalanImageActivity.this).mailAccount,"", "sj_image_view");
    }

    protected void handleMapComponents() {
        inflateLayout(R.layout.activity_stockout_detail_sj);
        urlImage = getIntent().getStringExtra(ARG_DATA);
        suratJalan = findViewById(R.id.stockout_detail_sj_pic);
        toolbar = findViewById(R.id.nonstockout_toolbar);
        printButton = findViewById(R.id.print_button);
        Glide.with(this)
                .load(urlImage)
                .into(suratJalan);
        toolbar.setNavigationIcon(null);
        toolbar.inflateMenu(R.menu.notification_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.notification_close) {
                    finish();
                }
                return false;
            }
        });
        View.OnClickListener onPrintClicked = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(SuratJalanImageActivity.this,AuthUtils.getCurrentUser(SuratJalanImageActivity.this).mailAccount,"", "sj_image_print");
                connectBTDevice();
            }
        };
        printButton.setOnClickListener(onPrintClicked);
    }

    private static BluetoothSocket btsocket;
    private static OutputStream btoutputstream;
    byte FONT_TYPE;

    protected void connectBTDevice() {
        if (btsocket == null) {
            Intent BTIntent = new Intent(getApplicationContext(), BTDeviceList.class);
            this.startActivityForResult(BTIntent, BTDeviceList.REQUEST_CONNECT_BT);
        } else {

            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            btoutputstream = opstream;
            printSuratJalan();

        }

    }
    private void printSuratJalan() {
        try {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            btoutputstream = btsocket.getOutputStream();

            byte[] printformat = {
                    0x1B,
                    0x21,
                    FONT_TYPE
            };
            btoutputstream.write(printformat);
            Bitmap bitmap = ((BitmapDrawable) suratJalan.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] suratJalanByte = baos.toByteArray();
            btoutputstream.write(suratJalanByte);
            btoutputstream.write(0x0D);
            btoutputstream.write(0x0D);
            btoutputstream.write(0x0D);
            btoutputstream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (btsocket != null) {
                btoutputstream.close();
                btsocket.close();
                btsocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            btsocket = BTDeviceList.getSocket();
            if (btsocket != null) {
                printSuratJalan();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}