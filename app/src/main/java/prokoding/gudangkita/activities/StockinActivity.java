package prokoding.gudangkita.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import io.github.muddz.styleabletoast.StyleableToast;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.Stockin;
import prokoding.gudangkita.models.Warehouse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockinActivity extends SubAPIActivity {
    private final ArrayList<String> optProduct = new ArrayList<>();
    private final ArrayList<String> optWarehouseLocation = new ArrayList<>();
    private AutoCompleteTextView product;
    private AutoCompleteTextView warehouseLocation;
    private TextInputEditText quantity;
    private MaterialButton submit;
    private ArrayList<Warehouse.Data> warehouseData;
    private ArrayList<Inventory.Data> inventoryData;

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_stockin, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.stockin_title));
        handleMapComponents();
        handleBindEvents();
        handlePrefetchData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(StockinActivity.this,AuthUtils.getCurrentUser(this).mailAccount,"", "stockin_view");
    }

    @Override
    protected void handleReload() {
        handlePrefetchData();
    }

    protected void handleMapComponents() {
        product = findViewById(R.id.stockin_product);
        warehouseLocation = findViewById(R.id.stockin_warehouse_location);
        quantity = findViewById(R.id.stockin_quantity);
        submit = findViewById(R.id.stockin_submit);
    }

    protected void handlePrefetchData() {
        // show loading
        switchLayoutState(STATE_SHIVER);

        // define request header
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);

        Call<Inventory> callInventory = ApiRetrofit.getApi(this).getAllProducts(appVersion, words, token);
        Call<Warehouse> callWarehouse = ApiRetrofit.getApi(this).getWarehouses(appVersion, words, token);

        callInventory.enqueue(new Callback<Inventory>() {
            @Override
            public void onResponse(Call<Inventory> call, Response<Inventory> response) {
                if (response.body().data != null) {
                    inventoryData = response.body().data;
                    handleOnDataPrefetched();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockinActivity.this,AuthUtils.getCurrentUser(StockinActivity.this).mailAccount,response.body().responseMessage, "stockin_inv_fetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Inventory> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
            }
        });
        callWarehouse.enqueue(new Callback<Warehouse>() {
            @Override
            public void onResponse(Call<Warehouse> call, Response<Warehouse> response) {
                if (response.body().data != null) {
                    warehouseData = response.body().data;
                    handleOnDataPrefetched();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockinActivity.this,AuthUtils.getCurrentUser(StockinActivity.this).mailAccount,response.body().responseMessage, "stockin_wrh_prefetch_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<Warehouse> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
            }
        });
    }

    protected void handleOnDataPrefetched() {
        if (inventoryData != null && warehouseData != null) {
            // show content
            switchLayoutState(STATE_CONTENT);

            // fill necessary dropdown
            optProduct.clear();
            optWarehouseLocation.clear();
            for (int i = 0; i < inventoryData.size(); i++) {
                optProduct.add(inventoryData.get(i).productName);
            }
            for (int i = 0; i < warehouseData.size(); i++) {
                optWarehouseLocation.add(warehouseData.get(i).warehouseName);
            }

            // bind the data to dropdown
            ArrayAdapter<String> productAdapter = new ArrayAdapter<>(StockinActivity.this, R.layout.item_dropdown, optProduct);
            ArrayAdapter<String> warehouseLocationAdapter = new ArrayAdapter<>(StockinActivity.this, R.layout.item_dropdown, optWarehouseLocation);
            product.setAdapter(productAdapter);
            warehouseLocation.setAdapter(warehouseLocationAdapter);
        }
    }

    protected void handleBindEvents() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                handleValidate();
            }
        };
        ImagePicker.OnFilePicked onFilePicked = new ImagePicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filepath) {
                handleValidate();
            }
        };
        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        };
        product.setOnItemClickListener(onDropdownChange);
        warehouseLocation.setOnItemClickListener(onDropdownChange);
        quantity.addTextChangedListener(onTextChange);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleSubmit() {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(StockinActivity.this,AuthUtils.getCurrentUser(StockinActivity.this).mailAccount,"", "stockin_submit_clicked");
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        ProgressDialogUtils.show(this);
        Call<Stockin> callStockin = ApiRetrofit.getApi(this).addStockin(
                appVersion,
                words,
                token,
                inventoryData.get(optProduct.indexOf(product.getText().toString())).productNumber,
                inventoryData.get(optProduct.indexOf(product.getText().toString())).productName,
                Integer.valueOf(quantity.getText().toString()),
                warehouseData.get(optWarehouseLocation.indexOf(warehouseLocation.getText().toString())).warehouseNumber,
                warehouseData.get(optWarehouseLocation.indexOf(warehouseLocation.getText().toString())).warehouseName,
                AuthUtils.getCurrentUser(StockinActivity.this).userId
        );
        callStockin.enqueue(new Callback<Stockin>() {
            @Override
            public void onResponse(Call<Stockin> call, Response<Stockin> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().data != null) {
                    handleClear();
                    handlePrefetchData();
                    StyleableToast.makeText(StockinActivity.this, "Stock in successfully addded", R.style.Theme_GudangKita_Toast_Success).show();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(StockinActivity.this,AuthUtils.getCurrentUser(StockinActivity.this).mailAccount,response.body().responseMessage, "stockin_submit_failed");
                }
                StyleableToast.makeText(StockinActivity.this, getString(R.string.hint_request_failed), R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<Stockin> call, Throwable t) {
                ProgressDialogUtils.dismiss();
                StyleableToast.makeText(StockinActivity.this, getString(R.string.hint_request_failed), R.style.Theme_GudangKita_Toast_Danger).show();
            }
        });
    }

    protected void handleClear() {
        product.setText("", false);
        warehouseLocation.setText("", false);
        quantity.setText("");
        handleValidate();
    }

    protected void handleValidate() {
        int quantityInt = 0;
        String quantityStr = quantity.getText().toString();
        if (!quantityStr.isEmpty()) {
            quantityInt = Integer.valueOf(quantityStr);
        }
        boolean valid = quantityInt > 0 &&
                optProduct.contains(product.getText().toString()) &&
                optWarehouseLocation.contains(warehouseLocation.getText().toString());
        submit.setEnabled(valid);
    }
}