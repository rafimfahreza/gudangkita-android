package prokoding.gudangkita.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;

import io.github.muddz.styleabletoast.StyleableToast;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberAddActivity extends SubActivity {
    private TextInputEditText memberEmail;
    private TextInputEditText name;
    private AutoCompleteTextView accountType;
    private MaterialButton submit;
    private String[] optAccountType;
    private boolean isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateLayout(R.layout.activity_member_add);
        setTitle(getString(R.string.ourteam_title));
        handleMapComponents();
        handleFillData();
        handleBindEventValidate();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(MemberAddActivity.this,AuthUtils.getCurrentUser(MemberAddActivity.this).mailAccount,"", "add_member_view");
    }

    protected void handleMapComponents() {
        isAdmin = AuthUtils.getCurrentUser(this).accountType >= 3;
        memberEmail = findViewById(R.id.member_add_email);
        name = findViewById(R.id.member_add_name);
        accountType = findViewById(R.id.member_add_account_type);
        submit = findViewById(R.id.member_add_submit);
        if (!isAdmin) {
            finish();
        }
    }

    protected void handleFillData() {
        optAccountType = getResources().getStringArray(R.array.value_account_type);
        ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optAccountType);
        accountType.setAdapter(accountTypeAdapter);
    }

    protected void handleBindEventValidate() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                handleValidate();
            }
        };
        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };
        ImagePicker.OnFilePicked onFilePicked = new ImagePicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filePath) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(MemberAddActivity.this,AuthUtils.getCurrentUser(MemberAddActivity.this).mailAccount,"", "add_member_clicked");
                String appVersion = AuthUtils.getAppversion(MemberAddActivity.this);
                String words = AuthUtils.getWords(MemberAddActivity.this);
                String token = AuthUtils.getToken(MemberAddActivity.this);
                ProgressDialogUtils.show(MemberAddActivity.this);
                Call<BaseResponse> call = ApiRetrofit.getApi(MemberAddActivity.this).addMember(
                        appVersion,
                        words,
                        token,
                        memberEmail.getText().toString(),
                        name.getText().toString(),
                        Arrays.asList(optAccountType).indexOf(accountType.getText().toString()) + 1
                );
                call.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        ProgressDialogUtils.dismiss();
                        if (response.body().responseCode != null) {
                            if (response.body().responseCode.equals("1433")) {
                                StyleableToast.makeText(MemberAddActivity.this, String.format("Member with code %s was already exist", memberEmail.getText().toString()), R.style.Theme_GudangKita_Toast_Danger).show();
                                return;
                            }
                            if (response.body().responseMessage.equalsIgnoreCase("success")) {
                                StyleableToast.makeText(MemberAddActivity.this, "Member successfully addded", R.style.Theme_GudangKita_Toast_Success).show();
                                handleClear();
                                return;
                            } else {
                                FirebaseHelper firebase = FirebaseHelper.getInstance();
                                firebase.hitFirebaseLogEvent(MemberAddActivity.this,AuthUtils.getCurrentUser(MemberAddActivity.this).mailAccount,response.body().responseMessage, "add_member_failed");
                            }
                        }
                        StyleableToast.makeText(MemberAddActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        StyleableToast.makeText(MemberAddActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                        ProgressDialogUtils.dismiss();
                        t.printStackTrace();
                    }
                });
            }
        };
        name.addTextChangedListener(onTextChange);
        memberEmail.addTextChangedListener(onTextChange);
        accountType.setOnItemClickListener(onDropdownChange);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleValidate() {
        boolean valid = !memberEmail.getText().toString().trim().isEmpty() &&
                !name.getText().toString().trim().isEmpty() &&
                Arrays.asList(optAccountType).contains(accountType.getText().toString());
        submit.setEnabled(valid);
    }

    protected void handleClear() {
        memberEmail.setText("");
        accountType.setText("", false);
        handleValidate();
    }

}