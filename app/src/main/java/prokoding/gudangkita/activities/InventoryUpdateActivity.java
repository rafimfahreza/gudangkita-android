package prokoding.gudangkita.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryUpdateActivity extends SubActivity {
    public static final String ARG_DATA = "DATA";
    private TextInputEditText itemCode;
    private TextInputEditText itemName;
    private TextInputEditText baseStock;
    private AutoCompleteTextView itemType;
    private ImagePicker imagePicker1;
    private MaterialButton submit;
    private ArrayList<String> optItemType;
    private boolean isAdmin;
    private Inventory.Data data;
    boolean isGrantedStorage = false;

    ActivityResultLauncher<String> permissionAskLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    isGrantedStorage = result;
                    if (isGrantedStorage) {
                        handleMapComponents();
                        handleBindEventValidate();
                        handleFillData();
                        return;
                    }
                    finish();
                }
            }
    );
    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        isGrantedStorage = Environment.isExternalStorageManager();
                        if (isGrantedStorage) {
                            handleMapComponents();
                            handleBindEventValidate();
                            handleFillData();
                            return;
                        }
                        finish();
                    }
                }
            }
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateLayout(R.layout.activity_inventory_add);
        setTitle(getString(R.string.inventories_title));
        if (!isGrantedStorage) {
            handlePermission();
        }
        if (isGrantedStorage) {
            handleMapComponents();
            handleBindEventValidate();
            handleFillData();
        }
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(InventoryUpdateActivity.this,AuthUtils.getCurrentUser(InventoryUpdateActivity.this).mailAccount,"", "update_inventory_view");
    }

    protected void handleMapComponents() {
        isAdmin = AuthUtils.getCurrentUser(this).accountType >= 3;
        if (!isAdmin) {
            finish();
        }
        itemCode = findViewById(R.id.inventoryadd_item_code);
        itemName = findViewById(R.id.inventoryadd_item_name);
        itemType = findViewById(R.id.inventoryadd_product_type);
        baseStock = findViewById(R.id.inventoryadd_item_base_stock);
        imagePicker1 = findViewById(R.id.inventoryadd_upload_1);
        submit = findViewById(R.id.inventoryadd_submit);
        data = (Inventory.Data) getIntent().getSerializableExtra(ARG_DATA);
        baseStock.setEnabled(false);
    }

    protected void handleFillData() {
        optItemType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_item_type)));
        ArrayAdapter<String> productTypeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optItemType);
        itemType.setAdapter(productTypeAdapter);
        itemCode.setText(data.productCode);
        baseStock.setText(String.valueOf(data.baseStock));
        imagePicker1.setImageByUrl(data.productImage);
        itemName.setText(data.productName);
        for (int i = 0; i < optItemType.size(); i++) {
            String type = "(" + optItemType.get(i) + ")";
            int index = data.productName.toLowerCase().indexOf(type.toLowerCase());
            if (index > -1) {
                String productName = data.productName.substring(0, index)
                        + data.productName.substring(index + type.length());
                itemName.setText(productName.trim());
                itemType.setText(optItemType.get(i), false);
                break;
            }
        }
    }

    protected void handleBindEventValidate() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                handleValidate();
            }
        };
        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };
        ImagePicker.OnFilePicked onFilePicked = new ImagePicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filePath) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        };
        itemCode.addTextChangedListener(onTextChange);
        itemName.addTextChangedListener(onTextChange);
        baseStock.addTextChangedListener(onTextChange);
        itemType.setOnItemClickListener(onDropdownChange);
        imagePicker1.setOnFilePicked(onFilePicked);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleSubmit() {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(InventoryUpdateActivity.this,AuthUtils.getCurrentUser(InventoryUpdateActivity.this).mailAccount,"", "update_inventory_clicked");
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        File file = new File(imagePicker1.getFilePath());
        ProgressDialogUtils.show(InventoryUpdateActivity.this);
        Call<BaseResponse> callStockin = ApiRetrofit.getApi(InventoryUpdateActivity.this).updateProduct(
                RequestBody.create(MultipartBody.FORM, appVersion),
                RequestBody.create(MultipartBody.FORM, words),
                RequestBody.create(MultipartBody.FORM, token),
                RequestBody.create(MultipartBody.FORM, String.valueOf(data.productNumber)),
                RequestBody.create(MultipartBody.FORM, itemCode.getText().toString().equals(data.productCode) ? "not set" : itemCode.getText().toString()),
                RequestBody.create(MultipartBody.FORM, itemName.getText().toString()),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optItemType.indexOf(itemType.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, baseStock.getText().toString()),
                MultipartBody.Part.createFormData("itemPicture", file.getName(),
                        RequestBody.create(
                                MediaType.parse(imagePicker1.getFilePath()),
                                file
                        )
                )
        );
        callStockin.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().responseCode != null) {
                    if (response.body().responseMessage.equalsIgnoreCase("success")) {
                        handleClear();
                        StyleableToast.makeText(InventoryUpdateActivity.this, "Item successfully updated", R.style.Theme_GudangKita_Toast_Success).show();
                        finish();
                        return;
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(InventoryUpdateActivity.this,AuthUtils.getCurrentUser(InventoryUpdateActivity.this).mailAccount,response.body().responseMessage, "update_inventory_failed");
                    }
                }
                StyleableToast.makeText(InventoryUpdateActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                StyleableToast.makeText(InventoryUpdateActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                ProgressDialogUtils.dismiss();
            }
        });
    }

    protected void handlePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            isGrantedStorage = Environment.isExternalStorageManager();
            if (!isGrantedStorage) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", this.getPackageName(), null);
                intent.setData(uri);
                Toast.makeText(this, R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                activityResultLauncher.launch(intent);
                return;
            }

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            isGrantedStorage = permission == PackageManager.PERMISSION_GRANTED;
            if (!isGrantedStorage) {
                Toast.makeText(this, R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                return;
            }
        }
    }

    protected void handleClear() {
        itemCode.setText("");
        itemName.setText("");
        baseStock.setText("");
        itemType.setText("", false);
        imagePicker1.removeImage();
        handleValidate();
    }

    protected void handleValidate() {
        boolean valid = !itemName.getText().toString().isEmpty() &&
                optItemType.contains(itemType.getText().toString()) &&
                imagePicker1.getFilePath() != null;
        submit.setEnabled(valid);
    }

}