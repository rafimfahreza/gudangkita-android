package prokoding.gudangkita.activities.templates;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import prokoding.gudangkita.R;

public abstract class APIActivity extends AppCompatActivity {
    public static @LayoutRes
    int SHIVER_LIST = R.layout.component_item_list_shiver;
    public static int SHIVER_FIELD = R.layout.component_item_field_shiver;
    public static int SHIVER_CARD = R.layout.component_item_card_shiver;
    public static int STATE_SHIVER = 1;
    public static int STATE_CONTENT = 2;
    public static int STATE_NODATA = 3;
    public static int STATE_FAILED = 4;
    protected Button reload;
    protected int currentFetchingState;
    private @NonNull
    @LayoutRes
    int mainLayout;
    private View shiver;
    private View content;
    private View nodata;
    private View failed;
    private ViewGroup container;

    protected abstract @NonNull
    @LayoutRes
    int getMainLayout();

    protected void handleReload() {
    }

    protected abstract View createContent();

    protected abstract ViewGroup createContainer();


    protected View createNodata() {
        return getLayoutInflater().inflate(R.layout.component_nodata, null);
    }

    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_LIST, null);
    }

    protected View createFailed() {
        return getLayoutInflater().inflate(R.layout.component_reload, null);
    }

    protected Button createFailedReloadButton() {
        return failed.findViewById(R.id.reload_button);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getMainLayout());
        initLayoutState();
    }

    protected void initLayoutState() {
        container = createContainer();
        shiver = createShiver();
        content = createContent();
        nodata = createNodata();
        failed = createFailed();
        container.removeAllViews();
        container.addView(shiver);
        container.addView(content);
        container.addView(failed);
        container.addView(nodata);
        reload = createFailedReloadButton();
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleReload();
            }
        });
    }

    protected void switchLayoutState(int state) {
        if (state == STATE_SHIVER) {
            currentFetchingState = STATE_SHIVER;
            shiver.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            failed.setVisibility(View.GONE);
            nodata.setVisibility(View.GONE);
            return;
        }
        if (state == STATE_CONTENT) {
            currentFetchingState = STATE_CONTENT;
            shiver.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            failed.setVisibility(View.GONE);
            nodata.setVisibility(View.GONE);
            return;
        }
        if (state == STATE_FAILED) {
            currentFetchingState = STATE_FAILED;
            shiver.setVisibility(View.GONE);
            content.setVisibility(View.GONE);
            failed.setVisibility(View.VISIBLE);
            nodata.setVisibility(View.GONE);
            return;
        }
        if (state == STATE_NODATA) {
            currentFetchingState = STATE_NODATA;
            shiver.setVisibility(View.GONE);
            content.setVisibility(View.GONE);
            failed.setVisibility(View.GONE);
            nodata.setVisibility(View.VISIBLE);
            return;
        }
    }
}
