package prokoding.gudangkita.activities.templates;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.MaterialToolbar;

import prokoding.gudangkita.R;

public class SubActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        handleNavigation();
    }

    protected void inflateLayout(@NonNull @LayoutRes int id) {
        LayoutInflater lI = getLayoutInflater();
        FrameLayout container = findViewById(R.id.nonstockout_container);
        container.removeAllViews();
        lI.inflate(id, container, true);
    }

    protected View addLayout(@NonNull @LayoutRes int id, ViewGroup viewGroup) {
        LayoutInflater lI = getLayoutInflater();
        View view = lI.inflate(id, null);
        viewGroup.addView(view);
        return view;
    }

    protected View addLayout(@NonNull @LayoutRes int id) {
        LayoutInflater lI = getLayoutInflater();
        FrameLayout container = findViewById(R.id.nonstockout_container);
        View view = lI.inflate(id, null);
        container.addView(view);
        return view;
    }

    protected void setTitle(String title) {
        MaterialToolbar toolbar = findViewById(R.id.nonstockout_toolbar);
        toolbar.setTitle(title);
    }

    private void handleNavigation() {
        MaterialToolbar toolbar = findViewById(R.id.nonstockout_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_slide_out_reverse, R.anim.right_slide_in_reverse);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_slide_out_reverse, R.anim.right_slide_in_reverse);
    }

    public void setAppbarBorderVisibility(int visibility) {
        View appbarBorder = findViewById(R.id.nonstockout_appbar_border);
        appbarBorder.setVisibility(visibility);
    }
}
