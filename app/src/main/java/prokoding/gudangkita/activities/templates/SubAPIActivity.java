package prokoding.gudangkita.activities.templates;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.material.appbar.MaterialToolbar;

import prokoding.gudangkita.R;

public abstract class SubAPIActivity extends APIActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleNavigation();
    }

    @NonNull
    @Override
    protected int getMainLayout() {
        return R.layout.activity_sub;
    }

    @Override
    protected ViewGroup createContainer() {
        return findViewById(R.id.nonstockout_container);
    }

    protected void setTitle(String title) {
        MaterialToolbar toolbar = findViewById(R.id.nonstockout_toolbar);
        toolbar.setTitle(title);
    }

    private void handleNavigation() {
        MaterialToolbar toolbar = findViewById(R.id.nonstockout_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_slide_out_reverse, R.anim.right_slide_in_reverse);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_slide_out_reverse, R.anim.right_slide_in_reverse);
    }

    public void setAppbarBorderVisibility(int visibility) {
        View appbarBorder = findViewById(R.id.nonstockout_appbar_border);
        appbarBorder.setVisibility(visibility);
    }
}
