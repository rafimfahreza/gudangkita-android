package prokoding.gudangkita.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.CustomerHistory;
import prokoding.gudangkita.models.RajaOngkirProvince;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerUpdateActivity extends SubAPIActivity {
    public static final String ARG_CUSTOMER = "ARG_CUSTOMER";
    public static final String ARG_APPROVED = "ARG_APPROVED";
    TextInputEditText name;
    TextInputEditText phoneNumber;
    AutoCompleteTextView idType;
    TextInputEditText idNumber;
    TextInputEditText address;
    AutoCompleteTextView province;
    AutoCompleteTextView type;
    AutoCompleteTextView approved;
    ImagePicker imagePicker1;
    MaterialButton submit;
    ArrayList<RajaOngkirProvince.Rajaongkir.Data> provinces;
    CustomerHistory.Data customer;
    ArrayList<String> optIdType = new ArrayList<>();
    ArrayList<String> optProvince = new ArrayList<>();
    ArrayList<String> optType = new ArrayList<>();
    ArrayList<String> optApproved = new ArrayList<>();
    TextInputLayout idTypeContainer;
    String customerId;
    int isApproved;
    boolean isGrantedStorage = false;
    ActivityResultLauncher<String> permissionAskLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    isGrantedStorage = result;
                    if (isGrantedStorage) {
                        handleMapComponents();
                        handlePrefetchData();
                        handleBindEvents();
                        return;
                    }
                    finish();
                }
            }
    );
    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        isGrantedStorage = Environment.isExternalStorageManager();
                        if (isGrantedStorage) {
                            handleMapComponents();
                            handlePrefetchData();
                            handleBindEvents();
                            return;
                        }
                        finish();
                    }
                }
            }
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.customers_title));
        if (!isGrantedStorage) {
            handlePermission();
        }
        if (isGrantedStorage) {
            handleMapComponents();
            handlePrefetchData();
            handleBindEvents();
        }
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(CustomerUpdateActivity.this,AuthUtils.getCurrentUser(CustomerUpdateActivity.this).mailAccount,"", "detail_cust_view");
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_customer_detail, null);
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    protected void handleMapComponents() {
        name = findViewById(R.id.customers_name);
        phoneNumber = findViewById(R.id.customers_phone);
        idType = findViewById(R.id.customers_id_type);
        idTypeContainer = findViewById(R.id.customers_id_type_container);
        idNumber = findViewById(R.id.customers_id_number);
        address = findViewById(R.id.customers_address);
        province = findViewById(R.id.customers_province);
        type = findViewById(R.id.customers_type);
        imagePicker1 = findViewById(R.id.customers_upload_1);
        submit = findViewById(R.id.customers_submit);
        approved = findViewById(R.id.customers_isApproved);
        customerId = getIntent().getStringExtra(ARG_CUSTOMER);
        isApproved = getIntent().getIntExtra(ARG_APPROVED, 0);
    }

    @Override
    protected void handleReload() {
        handlePrefetchData();
    }

    protected void handlePrefetchData() {
        switchLayoutState(STATE_SHIVER);
        optIdType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_id_type)));
        optType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_customer_type)));
        optApproved = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_isApproved)));

        ArrayAdapter<String> idTypeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optIdType);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optType);
        ArrayAdapter<String> approvedAdapter = new ArrayAdapter<>(this, R.layout.item_dropdown, optApproved);

        idType.setAdapter(idTypeAdapter);
        type.setAdapter(typeAdapter);
        approved.setAdapter(approvedAdapter);

        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);

        Call<RajaOngkirProvince> callProvince = ApiRetrofit.getRajaOngkirApi(this).getProvinces(getString(R.string.config_api_rajaoingkir_key));
        Call<CustomerHistory> callCustomer = ApiRetrofit.getApi(this).getCustomerDetails(appVersion, words, token, customerId);

        callProvince.enqueue(new Callback<RajaOngkirProvince>() {
            @Override
            public void onResponse(Call<RajaOngkirProvince> call, Response<RajaOngkirProvince> response) {
                if (response.body().rajaongkir != null) {
                    if (response.body().rajaongkir.results != null) {
                        provinces = response.body().rajaongkir.results;
                        handleOnResponse();
                        return;
                    }
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<RajaOngkirProvince> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });

        callCustomer.enqueue(new Callback<CustomerHistory>() {
            @Override
            public void onResponse(Call<CustomerHistory> call, Response<CustomerHistory> response) {
                if (response.body().data != null) {
                    customer = response.body().data;
                    handleOnResponse();
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(CustomerUpdateActivity.this,AuthUtils.getCurrentUser(CustomerUpdateActivity.this).mailAccount,response.body().responseMessage, "custdet_prefetch_failed");

                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<CustomerHistory> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handlePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            isGrantedStorage = Environment.isExternalStorageManager();
            if (!isGrantedStorage) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", this.getPackageName(), null);
                intent.setData(uri);
                Toast.makeText(this, R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                activityResultLauncher.launch(intent);
                return;
            }

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            isGrantedStorage = permission == PackageManager.PERMISSION_GRANTED;
            if (!isGrantedStorage) {
                Toast.makeText(this, R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                permissionAskLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                return;
            }
        }
    }

    protected void handleOnResponse() {
        if (provinces != null && customer != null) {
            switchLayoutState(STATE_CONTENT);
            for (int i = 0; i < provinces.size(); i++) {
                optProvince.add(provinces.get(i).province);
            }
            ArrayAdapter<String> provinceAdapter = new ArrayAdapter<>(CustomerUpdateActivity.this, R.layout.item_dropdown, optProvince);
            province.setAdapter(provinceAdapter);

            name.setText(customer.customerName);
            phoneNumber.setText(customer.customerPhone);
            idType.setText(customer.customerIdentityType.equalsIgnoreCase("NPWP") ? optIdType.get(1) : optIdType.get(0), false);
            idNumber.setText(customer.customerIdentityNumber);
            imagePicker1.setImageByUrl(customer.customerIdentityPic);
            address.setText(customer.customerAddress);
            province.setText(customer.province);
            type.setText(customer.customerType.equalsIgnoreCase("corporate") ? optType.get(1) : optType.get(0), false);
            approved.setText(optApproved.get(isApproved), false);
            submit.setEnabled(false);
        }

    }

    protected void handleBindEvents() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleValidate();
            }
        };

        AdapterView.OnItemClickListener onTypeChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (optType.get(position).equalsIgnoreCase("corporate")) {
                    idType.setText("NPWP", false);
                    idTypeContainer.setEnabled(false);
                    idType.setEnabled(false);
                } else if (!idType.getText().toString().isEmpty()) {
                    idType.setText("KTP", false);
                    idTypeContainer.setEnabled(true);
                    idType.setEnabled(true);
                }
                handleValidate();
            }
        };

        TextWatcher onTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleValidate();
            }
        };

        ImagePicker.OnFilePicked onFilePicked = new ImagePicker.OnFilePicked() {
            @Override
            public void onFilePicked(String filepath) {
                handleValidate();
            }
        };
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        };
        type.setOnItemClickListener(onTypeChange);
        name.addTextChangedListener(onTextChange);
        phoneNumber.addTextChangedListener(onTextChange);
        idType.setOnItemClickListener(onDropdownChange);
        idNumber.addTextChangedListener(onTextChange);
        address.addTextChangedListener(onTextChange);
        province.setOnItemClickListener(onDropdownChange);
        imagePicker1.setOnFilePicked(onFilePicked);
        approved.addTextChangedListener(onTextChange);
        submit.setOnClickListener(onSubmit);
    }

    protected void handleSubmit() {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(CustomerUpdateActivity.this,AuthUtils.getCurrentUser(CustomerUpdateActivity.this).mailAccount,"", "update_cust_clicked");
        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        File file = new File(imagePicker1.getFilePath());
        ProgressDialogUtils.show(this);
        Call<BaseResponse> call = ApiRetrofit.getApi(this).updateCustomer(
                RequestBody.create(MultipartBody.FORM, appVersion),
                RequestBody.create(MultipartBody.FORM, words),
                RequestBody.create(MultipartBody.FORM, token),
                RequestBody.create(MultipartBody.FORM, customerId),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optType.indexOf(type.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, name.getText().toString()),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optIdType.indexOf(idType.getText().toString()) + 1)),
                RequestBody.create(MultipartBody.FORM, idNumber.getText().toString()),
                RequestBody.create(MultipartBody.FORM, address.getText().toString()),
                RequestBody.create(MultipartBody.FORM, province.getText().toString()),
                RequestBody.create(MultipartBody.FORM, phoneNumber.getText().toString()),
                RequestBody.create(MultipartBody.FORM, String.valueOf(AuthUtils.getCurrentUser(CustomerUpdateActivity.this).userId)),
                RequestBody.create(MultipartBody.FORM, String.valueOf(optApproved.indexOf(approved.getText().toString()))),
                MultipartBody.Part.createFormData("identityPicture", file.getName(),
                        RequestBody.create(
                                MediaType.parse(imagePicker1.getFilePath()),
                                file
                        )
                )
        );
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().responseMessage != null) {
                    if (response.body().responseMessage.equalsIgnoreCase("success")) {
                        handleClear();
                        StyleableToast.makeText(CustomerUpdateActivity.this, "Customer successfully updated", R.style.Theme_GudangKita_Toast_Success).show();
                        finish();
                        return;
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(CustomerUpdateActivity.this,AuthUtils.getCurrentUser(CustomerUpdateActivity.this).mailAccount,response.body().responseMessage, "update_cust_failed");
                    }
                }
                StyleableToast.makeText(CustomerUpdateActivity.this, getString(R.string.hint_request_failed), R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                StyleableToast.makeText(CustomerUpdateActivity.this, getString(R.string.hint_request_failed), R.style.Theme_GudangKita_Toast_Danger).show();
                ProgressDialogUtils.dismiss();
                t.printStackTrace();
            }
        });
    }

    protected void handleClear() {
        type.setText("", false);
        name.setText("");
        phoneNumber.setText("");
        idType.setText("", false);
        idNumber.setText("");
        address.setText("");
        province.setText("", false);
        imagePicker1.removeImage();
        handleValidate();
    }

    protected void handleValidate() {
        boolean valid = !name.getText().toString().isEmpty() &&
                !phoneNumber.getText().toString().isEmpty() &&
                optIdType.contains(idType.getText().toString()) &&
                !idNumber.getText().toString().isEmpty() &&
                !address.getText().toString().isEmpty() &&
                optProvince.contains(province.getText().toString()) &&
                optType.contains(type.getText().toString()) &&
                imagePicker1.getFilePath() != null;
        submit.setEnabled(valid);
    }

}