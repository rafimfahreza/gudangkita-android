package prokoding.gudangkita.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import io.github.muddz.styleabletoast.StyleableToast;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.PendingStockout;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import prokoding.gudangkita.utils.TextUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingStockoutActivity extends SubAPIActivity {
    public static String ARG_REF_ID = "REF_ID";
    private AutoCompleteTextView id;
    private AutoCompleteTextView type;
    private AutoCompleteTextView customer;
    private AutoCompleteTextView product;
    private AutoCompleteTextView quantity;
    private AutoCompleteTextView date;
    private AutoCompleteTextView dueDate;
    private AutoCompleteTextView submitter;
    private AutoCompleteTextView price;
    private AutoCompleteTextView totalPrice;
    private AutoCompleteTextView totalPaid;
    private AutoCompleteTextView noncashType;
    private AutoCompleteTextView deliveryDate;
    private LinearLayout nonCashFields;
    private TextView title;
    private TextView counter;
    private TextView labelTotalPrice;
    private MaterialDatePicker datePicker;
    private MaterialPickerOnPositiveButtonClickListener datePickerPositiveListener;
    private ArrayList<String> optNonCashType;
    private ArrayList<PendingStockout.Data> data;
    private MaterialButton submit;
    private String refId;
    private int position = 1;
    private long dueDateTimestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.pendinglist_title));
        handleMapComponents();
        handleFillData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(PendingStockoutActivity.this,AuthUtils.getCurrentUser(PendingStockoutActivity.this).mailAccount,"", "pend_stockout_view");
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_pending_stockout, null);
    }

    @Override
    protected void handleReload() {
        handleFillData();
    }

    protected void handleMapComponents() {
        id = findViewById(R.id.pending_stockout_id);
        quantity = findViewById(R.id.pending_stockout_quantity);
        type = findViewById(R.id.pending_stockout_type);
        customer = findViewById(R.id.pending_stockout_customer_name);
        product = findViewById(R.id.pending_stockout_product);
        title = findViewById(R.id.pending_stockout_title);
        price = findViewById(R.id.pending_stockout_required_price);
        totalPaid = findViewById(R.id.pending_stockout_total_paid);
        totalPrice = findViewById(R.id.pending_stockout_total_price);
        dueDate = findViewById(R.id.pending_stockout_due);
        date = findViewById(R.id.pending_stockout_order_date);
        deliveryDate = findViewById(R.id.stockout_delivery_date);
        labelTotalPrice = findViewById(R.id.pending_stockout_label_total_price);
        submit = findViewById(R.id.pending_stockout_submit);
        submitter = findViewById(R.id.pending_stockout_submitter);
        counter = findViewById(R.id.pending_stockout_counter);
        nonCashFields = findViewById(R.id.pending_stockout_noncash_fields);
        noncashType = findViewById(R.id.pending_stockout_noncash_type);
        optNonCashType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.value_noncash_type)));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_dropdown, optNonCashType);
        noncashType.setAdapter(adapter);
        datePicker = MaterialDatePicker.Builder.datePicker().build();
        refId = getIntent().getStringExtra(ARG_REF_ID);
        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    handleDateOrder((EditText) v);
                }
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDateOrder((EditText) v);
            }
        });

        deliveryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if (isFocus) {
                    handleDateOrder((EditText) view);
                }
            }
        });

        deliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleDateOrder((EditText) view);
            }
        });

        dueDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    handleDateDue();
                }
            }
        });
        dueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDateDue();
            }
        });
    }

    protected void handleDateOrder(EditText textField) {
        try {
            datePicker.removeOnPositiveButtonClickListener(datePickerPositiveListener);
        } finally {
            datePickerPositiveListener = new MaterialPickerOnPositiveButtonClickListener() {
                @Override
                public void onPositiveButtonClick(Object selection) {
                    long timestamp = Long.parseLong(selection.toString());
                    Date newDate = new Date(timestamp);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    textField.setText(sdf.format(newDate));
                }
            };
            datePicker.addOnPositiveButtonClickListener(datePickerPositiveListener);
            datePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
        }
    }

    protected void handleDateDue() {
        try {
            datePicker.removeOnPositiveButtonClickListener(datePickerPositiveListener);
        } finally {
            datePickerPositiveListener = new MaterialPickerOnPositiveButtonClickListener() {
                @Override
                public void onPositiveButtonClick(Object selection) {
                    long timestamp = Long.parseLong(selection.toString());
                    dueDateTimestamp = timestamp;
                    Date date = new Date(timestamp);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    dueDate.setText(sdf.format(date));
                }
            };
            datePicker.addOnPositiveButtonClickListener(datePickerPositiveListener);
            datePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
        }
    }

    protected void handleFillData() {
        switchLayoutState(STATE_SHIVER);

        String appVersion = AuthUtils.getAppversion(this);
        String words = AuthUtils.getWords(this);
        String token = AuthUtils.getToken(this);
        Call<PendingStockout> call = ApiRetrofit.getApi(this).getPendingStockout(appVersion, words, token, refId, "out");
        call.enqueue(new Callback<PendingStockout>() {
            @Override
            public void onResponse(Call<PendingStockout> call, Response<PendingStockout> response) {
                if (response.body().data != null) {
                    data = response.body().data;
                    position = 1;
                    handleBindEvent();
                    handleLoad(position);
                    switchLayoutState(STATE_CONTENT);
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(PendingStockoutActivity.this,AuthUtils.getCurrentUser(PendingStockoutActivity.this).mailAccount,response.body().responseMessage, "pend_stockout_failed");

                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<PendingStockout> call, Throwable t) {
                switchLayoutState(STATE_FAILED);
                t.printStackTrace();
            }
        });
    }

    protected void handleLoad(int position) {
        if (data.size() == 0) {
            finish();
            return;
        }
        this.position = position;
        PendingStockout.Data item = data.get(position - 1);
        id.setText(item.stockoutId);
        type.setText(item.paymentType);
        customer.setText(item.customerName);
        product.setText(item.productName);
        quantity.setText(String.valueOf(item.productQuantity));
        deliveryDate.setText(item.deliveryDate);
        submitter.setText(item.memberName);
        price.setText("");
        totalPrice.setText("");
        totalPaid.setText("", false);
        if (position == 1) {
            date.setText(item.stockoutDate);
            dueDate.setText("", false);
            date.setEnabled(true);
            dueDate.setEnabled(true);
        } else {
            date.setEnabled(false);
            dueDate.setEnabled(false);
        }
        if (item.paymentType.equalsIgnoreCase("cash")) {
            nonCashFields.setVisibility(View.GONE);
            title.setText("Stock Out Cash Payment");
            labelTotalPrice.setText("Total Price");
        } else {
            nonCashFields.setVisibility(View.VISIBLE);
            title.setText("Stock Out Non Cash Payment");
            labelTotalPrice.setText("Remaining Paid");
        }
        if (data.size() > 1) {
            String counterStr = String.format("(%d/%d)", position, data.size());
            counter.setVisibility(View.VISIBLE);
            counter.setText(counterStr);
            submit.setText("Update " + counterStr);
        } else {
            counter.setVisibility(View.GONE);
            counter.setText("(1/1)");
            submit.setText("Update");
        }
    }

    protected void handleSubmit(boolean isNonCash) {
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(PendingStockoutActivity.this,AuthUtils.getCurrentUser(PendingStockoutActivity.this).mailAccount,"", "pend_stockout_clicked");
        try {
            Call<BaseResponse> call;
            String appVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            String words = AuthUtils.getWords(this);
            String token = AuthUtils.getToken(this);
            ProgressDialogUtils.show(PendingStockoutActivity.this);
            Log.d("PRICE:", TextUtils.parseCurrencyValue(price.getText().toString()).toString());
            Log.d("TOTAL PAID:", TextUtils.parseCurrencyValue(totalPaid.getText().toString()).toString());
            Log.d("TOTAL PRICE:", TextUtils.parseCurrencyValue(totalPrice.getText().toString()).toString());

            if (!isNonCash) {
                call = ApiRetrofit.getApi(this).updatePendingStockoutCash(
                        appVersion,
                        words,
                        token,
                        refId,
                        data.get(position - 1).productId,
                        TextUtils.parseCurrencyValue(price.getText().toString()).longValue(),
                        TextUtils.parseCurrencyValue(totalPrice.getText().toString()).longValue(),
                        data.get(position - 1).productQuantity,
                        data.get(position - 1).stockoutDate,
                        true,
                        AuthUtils.getCurrentUser(PendingStockoutActivity.this).userId,
                        deliveryDate.getText().toString()
                );
            } else {
                Log.d("TOTAL AMOUNT:", String.valueOf(data.get(position - 1).productQuantity * TextUtils.parseCurrencyValue(price.getText().toString()).longValue()));
                call = ApiRetrofit.getApi(this).updatePendingStockoutNonCash(
                        appVersion,
                        words,
                        token,
                        refId,
                        data.get(position - 1).stockoutNumber,
                        data.get(position - 1).productId,
                        optNonCashType.indexOf(noncashType.getText().toString()) + 1,
                        TextUtils.parseCurrencyValue(price.getText().toString()).longValue(),
                        data.get(position - 1).productQuantity * TextUtils.parseCurrencyValue(price.getText().toString()).longValue(),
                        TextUtils.parseCurrencyValue(totalPaid.getText().toString()).longValue(),
                        TextUtils.parseCurrencyValue(totalPrice.getText().toString()).longValue(),
                        data.get(position - 1).productQuantity,
                        data.get(position - 1).stockoutDate,
                        dueDateTimestamp,
                        true,
                        AuthUtils.getCurrentUser(PendingStockoutActivity.this).userId,
                        deliveryDate.getText().toString()
                );
            }
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    ProgressDialogUtils.dismiss();
                    if (response.body() != null) {
                        if (response.body().responseMessage.equalsIgnoreCase("success")) {
                            StyleableToast.makeText(PendingStockoutActivity.this, "Item successfully updated", R.style.Theme_GudangKita_Toast_Success).show();
                            if (position < data.size()) {
                                position++;
                                handleLoad(position);
                                return;
                            }
                            handleFinalSubmit();
                            return;
                        } else {
                            FirebaseHelper firebase = FirebaseHelper.getInstance();
                            firebase.hitFirebaseLogEvent(PendingStockoutActivity.this,AuthUtils.getCurrentUser(PendingStockoutActivity.this).mailAccount,response.body().responseMessage, "pend_stockout_failed");

                        }
                    }
                    Log.d("TAG", "RESPONSE MESSAGE "+response.body().responseMessage);
                    StyleableToast.makeText(PendingStockoutActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    ProgressDialogUtils.dismiss();
                    Log.d("TAG", "Error occured "+t.toString());
                    StyleableToast.makeText(PendingStockoutActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                }
            });

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void handleFinalSubmit() {
        try {
            String appVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            String words = AuthUtils.getWords(this);
            String token = AuthUtils.getToken(this);
            ProgressDialogUtils.show(PendingStockoutActivity.this, "Updating status");
            Call<BaseResponse> call = ApiRetrofit.getApi(this).updatePendingStockoutGeneral(
                    appVersion,
                    words,
                    token,
                    refId
            );
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    ProgressDialogUtils.dismiss();
                    if (response.body() != null) {
                        if (response.body().responseMessage.equalsIgnoreCase("success")) {
                            StyleableToast.makeText(PendingStockoutActivity.this, "Stockout successfully updated", R.style.Theme_GudangKita_Toast_Success).show();
                            finish();
                            return;
                        } else {
                            FirebaseHelper firebase = FirebaseHelper.getInstance();
                            firebase.hitFirebaseLogEvent(PendingStockoutActivity.this,AuthUtils.getCurrentUser(PendingStockoutActivity.this).mailAccount,response.body().responseMessage, "pend_stockoutfin_failed");
                        }
                    }
                    StyleableToast.makeText(PendingStockoutActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    ProgressDialogUtils.dismiss();
                    StyleableToast.makeText(PendingStockoutActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                }
            });

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void handleBindEvent() {
        AdapterView.OnItemClickListener onDropdownChange = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean isNonCash = !data.get(position - 1).paymentType.equalsIgnoreCase("cash");
                handleValidate(isNonCash);
            }
        };
        TextWatcher sumTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean isNonCash = !data.get(position - 1).paymentType.equalsIgnoreCase("cash");
                handleSum(isNonCash);
                handleValidate(isNonCash);
            }
        };
        TextWatcher generalTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean isNonCash = !data.get(position - 1).paymentType.equalsIgnoreCase("cash");
                handleValidate(isNonCash);
            }
        };

        price.addTextChangedListener(sumTextWatcher);
        price.addTextChangedListener(handleCurrency(price));

        totalPrice.addTextChangedListener(generalTextWatcher);
        totalPrice.addTextChangedListener(handleCurrency(totalPrice));

        totalPaid.addTextChangedListener(sumTextWatcher);
        totalPaid.addTextChangedListener(handleCurrency(totalPaid));

        dueDate.addTextChangedListener(generalTextWatcher);

        boolean isNonCash = !data.get(position - 1).paymentType.equalsIgnoreCase("cash");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit(isNonCash);
            }
        });
    }

    protected TextWatcher handleCurrency(EditText editText) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText == null || editText.getText().toString().equals("")) {
                    return;
                }
                editText.removeTextChangedListener(this);
                BigDecimal parsed = TextUtils.parseCurrencyValue(editText.getText().toString());
                String formatted = TextUtils.parseCurrencyString(parsed);
                editText.setText(formatted);
                editText.setSelection(formatted.length());
                editText.addTextChangedListener(this);
            }
        };
    }

    protected void handleSum(boolean isNonCash) {
        long price;
        int quantity;
        long totalPrice;
        try {
            price = TextUtils.parseCurrencyValue(this.price.getText().toString()).longValue();
            quantity = Integer.parseInt(this.quantity.getText().toString());
            totalPrice = quantity * price;
            try {
                if (isNonCash) {
                    long totalPaid = TextUtils.parseCurrencyValue(this.totalPaid.getText().toString()).longValue();
                    totalPrice = Math.max(0, totalPrice - totalPaid);
                }
            } catch (Exception ignore) {
            }
            this.totalPrice.setText(TextUtils.parseCurrencyString(BigDecimal.valueOf(totalPrice)));
        } catch (Exception e) {
            this.totalPrice.setText(TextUtils.parseCurrencyString(BigDecimal.ZERO));
        }
    }

    protected void handleValidate(boolean isNonCash) {
        boolean isValid = !price.getText().toString().trim().isEmpty() &&
                !totalPrice.getText().toString().trim().isEmpty() && !deliveryDate.getText().toString().isEmpty();
        if (isNonCash) {
            isValid = isValid && !totalPaid.getText().toString().trim().isEmpty() &&
                    !dueDate.getText().toString().trim().isEmpty();
        }
        submit.setEnabled(isValid);
    }
}