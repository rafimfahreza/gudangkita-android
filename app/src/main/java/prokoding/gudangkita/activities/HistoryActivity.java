package prokoding.gudangkita.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.HorizontalScrollView;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.color.MaterialColors;

import java.util.ArrayList;

import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.adapters.PageAdapter;
import prokoding.gudangkita.fragments.CustomerHistoryFragment;
import prokoding.gudangkita.fragments.InventoryHistoryFragment;
import prokoding.gudangkita.fragments.StockinHistoryFragment;
import prokoding.gudangkita.fragments.StockoutHistoryFragment;
import prokoding.gudangkita.models.Pending;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;

public class HistoryActivity extends SubActivity {
    private PageAdapter pageAdapter;
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private final ArrayList<Pending.Data> pendingData = new ArrayList<>();
    private MaterialButton btnStockout;
    private MaterialButton btnStockin;
    private MaterialButton btnCustomer;
    private MaterialButton btnProducts;
    private HorizontalScrollView navigation;
    private ViewPager2 pager;
    private boolean isAdmin;
    private boolean isAccounting;
    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.history_title));
        handleMapComponents();
        handleFillData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(HistoryActivity.this,AuthUtils.getCurrentUser(this).mailAccount,"", "history_view");
    }

    protected void handleMapComponents() {
        inflateLayout(R.layout.activity_history);
        isAccounting = AuthUtils.getCurrentUser(this).accountType >= 2;
        isAdmin = AuthUtils.getCurrentUser(this).accountType >= 3;
        btnStockout = findViewById(R.id.pending_button_stockout);
        btnStockin = findViewById(R.id.pending_button_stockin);
        btnProducts = findViewById(R.id.pending_button_products);
        btnCustomer = findViewById(R.id.pending_button_customers);
        navigation = findViewById(R.id.pending_navigation);
        pager = findViewById(R.id.pending_pager);

        if (!isAccounting) {
            finish();
            return;
        }
        if (!isAdmin) {
            btnCustomer.setVisibility(View.GONE);
        }
    }

    protected void handleFillData() {
        setAppbarBorderVisibility(View.GONE);
        fragments = new ArrayList<>();
        fragments.add(new StockoutHistoryFragment());
        fragments.add(new StockinHistoryFragment());
        fragments.add(new InventoryHistoryFragment());
        if (isAdmin) {
            fragments.add(new CustomerHistoryFragment());
        }
        pageAdapter = new PageAdapter(this, fragments);
        pager.setAdapter(pageAdapter);
        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                currentPosition = position;
                if (position == 0) {
                    navigation.smoothScrollTo((int) btnStockout.getX() - (btnStockout.getWidth() / 2), 0);
                    setBtnActive(btnStockout);
                    setBtnInactive(btnStockin);
                    setBtnInactive(btnCustomer);
                    setBtnInactive(btnProducts);
                } else if (position == 1) {
                    navigation.smoothScrollTo((int) btnStockin.getX() - (btnStockin.getWidth() / 2), 0);
                    setBtnInactive(btnStockout);
                    setBtnActive(btnStockin);
                    setBtnInactive(btnCustomer);
                    setBtnInactive(btnProducts);
                } else if (position == 2) {
                    navigation.smoothScrollTo((int) btnProducts.getX() - (btnProducts.getWidth() / 2), 0);
                    setBtnInactive(btnStockout);
                    setBtnInactive(btnStockin);
                    setBtnInactive(btnCustomer);
                    setBtnActive(btnProducts);
                } else {
                    navigation.smoothScrollTo((int) btnCustomer.getX() - (btnCustomer.getWidth() / 2), 0);
                    setBtnInactive(btnStockout);
                    setBtnInactive(btnStockin);
                    setBtnInactive(btnProducts);
                    setBtnActive(btnCustomer);
                }
            }
        });
        handleBindButtonToPager(btnStockout, 0);
        handleBindButtonToPager(btnStockin, 1);
        handleBindButtonToPager(btnProducts, 2);
        handleBindButtonToPager(btnCustomer, 3);
        navigation.setVisibility(View.VISIBLE);
        pager.setCurrentItem(currentPosition, false);
    }


    protected void handleBindButtonToPager(MaterialButton button, int position) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = position;
                pager.setCurrentItem(position, true);
            }
        });
    }

    protected void setBtnActive(MaterialButton button) {
        button.setBackgroundColor(MaterialColors.getColor(this, R.attr.colorPrimary, getResources().getColor(R.color.red)));
        button.setTextColor(MaterialColors.getColor(this, R.attr.colorOnPrimary, Color.WHITE));
    }

    protected void setBtnInactive(MaterialButton button) {
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setTextAppearance(this, R.style.Theme_GudangKita_TextView_Body);
        button.setTextColor(MaterialColors.getColor(this, R.attr.colorPrimary, getResources().getColor(R.color.red)));
    }
}
