package prokoding.gudangkita.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;

import java.io.File;

import io.github.muddz.styleabletoast.StyleableToast;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubActivity;
import prokoding.gudangkita.adapters.SjProductAdapter;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.Stockout;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.DrawableUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuratJalanActivity extends SubActivity {
    public static final String ARG_DATA = "DATA";
    public static final String ARG_SHIPPING = "SHIPPING";
    private static final int UPLOAD_STATE_NEUTRAL = 0;
    private static final int UPLOAD_STATE_SENT = 1;
    private static final int UPLOAD_STATE_FAILED = 2;
    String shippingMethod = "";
    String filename;
    private Stockout.Data data;
    private TextView sj;
    private TextView name;
    private TextView address;
    private TextView shipping;
    private TextView date;
    private RecyclerView products;
    private MaterialButton download;
    private NestedScrollView scrollView;
    private MaterialToolbar toolbar;
    private int uploadState = UPLOAD_STATE_NEUTRAL;
    protected ActivityResultLauncher<String> permissionAskLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    if (result) {
                        uploadImage();
                    }
                }
            }
    );
    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (Environment.isExternalStorageManager()) {
                            uploadImage();
                        }
                    }
                }
            }
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateLayout(R.layout.activity_surat_jalan);
        setTitle("Surat Jalan");
        handleMapComponents();
        handleFillData();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(SuratJalanActivity.this,AuthUtils.getCurrentUser(SuratJalanActivity.this).mailAccount,"", "sj_stockout_view");
    }

    protected void handleMapComponents() {
        data = (Stockout.Data) getIntent().getSerializableExtra(ARG_DATA);
        sj = findViewById(R.id.sj_number);
        name = findViewById(R.id.sj_name);
        address = findViewById(R.id.sj_address);
        shipping = findViewById(R.id.sj_shipping);
        date = findViewById(R.id.sj_date);
        products = findViewById(R.id.sj_products);
        download = findViewById(R.id.sj_download);
        scrollView = findViewById(R.id.sj_scrollview);
        toolbar = findViewById(R.id.nonstockout_toolbar);
        toolbar.setNavigationIcon(null);
        toolbar.inflateMenu(R.menu.notification_menu);
        toolbar.getMenu().getItem(0).setVisible(false);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.notification_close) {
                    handleFinishActivity();
                }
                return false;
            }
        });
    }

    protected void handleFillData() {
        Intent intent = getIntent();
        Stockout.Data data = (Stockout.Data) intent.getSerializableExtra(ARG_DATA);
        shippingMethod = intent.getStringExtra(ARG_SHIPPING);
        sj.setText(data.sjNumber);
        name.setText(data.customerName);
        address.setText(data.address);
        shipping.setText(shippingMethod);
        date.setText(data.orderDate);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        SjProductAdapter adapter = new SjProductAdapter(data.products, new String[]{"METER", "PCS"});
        products.setLayoutManager(linearLayoutManager);
        products.setAdapter(adapter);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper firebase = FirebaseHelper.getInstance();
                firebase.hitFirebaseLogEvent(SuratJalanActivity.this,AuthUtils.getCurrentUser(SuratJalanActivity.this).mailAccount,"", "sj_download_click");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    if (!Environment.isExternalStorageManager()) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        Toast.makeText(SuratJalanActivity.this, R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                        activityResultLauncher.launch(intent);
                        return;
                    }
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int permission = ActivityCompat.checkSelfPermission(SuratJalanActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(SuratJalanActivity.this, R.string.hint_allow_storage, Toast.LENGTH_LONG).show();
                        permissionAskLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        return;
                    }
                }
                uploadImage();
            }
        });

    }

    protected void saveImage() {
        try {
            Bitmap bitmap = DrawableUtils.getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
            filename = DrawableUtils.writeBitmapToFile(bitmap, data.sjNumber);
            if (filename != null) {
                toolbar.getMenu().getItem(0).setVisible(true);
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + filename)));
                StyleableToast.makeText(SuratJalanActivity.this, "Document saved to " + filename, R.style.Theme_GudangKita_Toast_Success).show();
            }
        } catch (Exception e) {
            StyleableToast.makeText(SuratJalanActivity.this, "Sorry the document wasn't able to be saved to the local storage, you can screenshot it manually instead", R.style.Theme_GudangKita_Toast_Danger).show();
            e.printStackTrace();
        }
    }

    protected void uploadImage() {
        if (uploadState != UPLOAD_STATE_SENT) {
            ProgressDialogUtils.show(this, "Uploading letter");
            Bitmap bitmap = DrawableUtils.getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
            String temp = DrawableUtils.writeBitmapToTemp(bitmap, data.sjNumber);
            File file = new File(temp);
            String appVersion = AuthUtils.getAppversion(this);
            String words = AuthUtils.getWords(this);
            String token = AuthUtils.getToken(this);
            Call<BaseResponse> call = ApiRetrofit.getApi(this).uploadSJ(
                    RequestBody.create(MultipartBody.FORM, appVersion),
                    RequestBody.create(MultipartBody.FORM, words),
                    RequestBody.create(MultipartBody.FORM, token),
                    RequestBody.create(MultipartBody.FORM, data.sjNumber),
                    MultipartBody.Part.createFormData("sjPicture", file.getName(),
                            RequestBody.create(
                                    MediaType.parse(temp),
                                    file
                            )
                    )
            );
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.body() != null) {
                        if (response.body().responseMessage.equalsIgnoreCase("success")) {
                            ProgressDialogUtils.dismiss();
                            StyleableToast.makeText(SuratJalanActivity.this, "The letter successfully uploaded", R.style.Theme_GudangKita_Toast_Success).show();
                            if (uploadState == UPLOAD_STATE_FAILED) {
                                uploadState = UPLOAD_STATE_SENT;
                                saveImage();
                                file.delete();
                                return;
                            }
                            uploadState = UPLOAD_STATE_SENT;
                            saveImage();
                            file.delete();
                            return;
                        } else {
                            FirebaseHelper firebase = FirebaseHelper.getInstance();
                            firebase.hitFirebaseLogEvent(SuratJalanActivity.this,AuthUtils.getCurrentUser(SuratJalanActivity.this).mailAccount,response.body().responseMessage, "sj_upload_failed");
                        }
                        file.delete();
                        uploadState = UPLOAD_STATE_FAILED;
                        StyleableToast.makeText(SuratJalanActivity.this, "The letter was not uploaded, please try again!", R.style.Theme_GudangKita_Toast_Danger).show();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    file.delete();
                    uploadState = UPLOAD_STATE_FAILED;
                    StyleableToast.makeText(SuratJalanActivity.this, "The letter was not uploaded, please try again!", R.style.Theme_GudangKita_Toast_Danger).show();
                    t.printStackTrace();
                }
            });
            return;
        }
        saveImage();
    }

    protected void handleFinishActivity() {
        if (filename != null && uploadState == UPLOAD_STATE_SENT) {
            super.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        handleFinishActivity();
    }
}