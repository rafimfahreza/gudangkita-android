package prokoding.gudangkita.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import io.github.muddz.styleabletoast.StyleableToast;
import prokoding.gudangkita.R;
import prokoding.gudangkita.activities.templates.SubAPIActivity;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.components.ImagePicker;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.PendingCustomer;
import prokoding.gudangkita.utils.AuthUtils;
import prokoding.gudangkita.utils.FirebaseHelper;
import prokoding.gudangkita.utils.ProgressDialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingCustomerActivity extends SubAPIActivity {
    public static String ARG_REF_ID = "REF_ID";
    private TextInputEditText name;
    private TextInputEditText phoneNumber;
    private AutoCompleteTextView idType;
    private TextInputEditText idNumber;
    private TextInputEditText address;
    private AutoCompleteTextView province;
    private AutoCompleteTextView type;
    private ImagePicker imagePicker1;
    private MaterialButton submit;
    private MaterialButton reject;
    private final ArrayList<String> optIdType = new ArrayList<>();
    private final ArrayList<String> optProvince = new ArrayList<>();
    private final ArrayList<String> optType = new ArrayList<>();
    private TextInputLayout idTypeContainer;
    private AutoCompleteTextView id;
    private String refId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.pendinglist_title));
        handleMapComponents();
        handleFillData();
        handleBindEventValidate();
        FirebaseHelper firebase = FirebaseHelper.getInstance();
        firebase.hitFirebaseLogEvent(PendingCustomerActivity.this,AuthUtils.getCurrentUser(PendingCustomerActivity.this).mailAccount,"", "update_cust_view");
    }

    @Override
    protected View createShiver() {
        return getLayoutInflater().inflate(SHIVER_FIELD, null);
    }

    @Override
    protected View createContent() {
        return getLayoutInflater().inflate(R.layout.activity_pending_customer, null);
    }

    protected void handleMapComponents() {
        name = findViewById(R.id.pending_customers_name);
        phoneNumber = findViewById(R.id.pending_customers_phone);
        idType = findViewById(R.id.pending_customers_id_type);
        idTypeContainer = findViewById(R.id.pending_customers_id_type_container);
        idNumber = findViewById(R.id.pending_customers_id_number);
        address = findViewById(R.id.pending_customers_address);
        province = findViewById(R.id.pending_customers_province);
        type = findViewById(R.id.pending_customers_type);
        imagePicker1 = findViewById(R.id.pending_customers_upload_1);
        submit = findViewById(R.id.pending_customers_submit);
        reject = findViewById(R.id.pending_customers_reject);
        id = findViewById(R.id.pending_customer_id);
        refId = getIntent().getStringExtra(ARG_REF_ID);
    }

    @Override
    protected void handleReload() {
        handleFillData();
    }

    protected void handleFillData() {
        switchLayoutState(STATE_SHIVER);
        String appVersion = AuthUtils.getAppversion(PendingCustomerActivity.this);
        String words = AuthUtils.getWords(PendingCustomerActivity.this);
        String token = AuthUtils.getToken(PendingCustomerActivity.this);
        Call<PendingCustomer> call = ApiRetrofit.getApi(this).getPendingCustomer(appVersion, words, token, refId, "cust");
        call.enqueue(new Callback<PendingCustomer>() {
            @Override
            public void onResponse(Call<PendingCustomer> call, Response<PendingCustomer> response) {
                if (response.body().data != null) {
                    PendingCustomer.Data data = response.body().data;
                    name.setText(data.customerName);
                    id.setText(refId);
                    phoneNumber.setText(data.customerPhone);
                    idType.setText(data.customerIdentityType);
                    idNumber.setText(data.customerIdentityNumber);
                    imagePicker1.setImageByUrl(data.customerIdentityPic, false);
                    address.setText(data.customerAddress);
                    province.setText(data.province);
                    type.setText(data.customerType);
                    switchLayoutState(STATE_CONTENT);
                    return;
                } else {
                    FirebaseHelper firebase = FirebaseHelper.getInstance();
                    firebase.hitFirebaseLogEvent(PendingCustomerActivity.this,AuthUtils.getCurrentUser(PendingCustomerActivity.this).mailAccount,response.body().responseMessage, "cust_upd_pref_failed");
                }
                switchLayoutState(STATE_FAILED);
            }

            @Override
            public void onFailure(Call<PendingCustomer> call, Throwable t) {
                t.printStackTrace();
                switchLayoutState(STATE_FAILED);
            }
        });

    }

    protected void handleBindEventValidate() {
        View.OnClickListener onSubmit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit(true);
            }
        };
        View.OnClickListener onReject = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit(false);
            }
        };
        submit.setOnClickListener(onSubmit);
        reject.setOnClickListener(onReject);
    }

    protected void handleSubmit(boolean isApproved) {
        String appVersion = AuthUtils.getAppversion(PendingCustomerActivity.this);
        String words = AuthUtils.getWords(PendingCustomerActivity.this);
        String token = AuthUtils.getToken(PendingCustomerActivity.this);
        ProgressDialogUtils.show(PendingCustomerActivity.this);
        Call<BaseResponse> call = ApiRetrofit.getApi(PendingCustomerActivity.this).updatePendingCustomer(
                appVersion,
                words,
                token,
                refId,
                isApproved ? 1 : 0,
                AuthUtils.getCurrentUser(PendingCustomerActivity.this).userId
        );
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                ProgressDialogUtils.dismiss();
                if (response.body().responseMessage != null) {
                    if (response.body().responseMessage.equals("SUCCESS")) {
                        String message = isApproved ? "Customer successfully approved" : "Customer successfully rejected";
                        StyleableToast.makeText(PendingCustomerActivity.this, message, R.style.Theme_GudangKita_Toast_Success).show();
                        finish();
                        return;
                    } else {
                        FirebaseHelper firebase = FirebaseHelper.getInstance();
                        firebase.hitFirebaseLogEvent(PendingCustomerActivity.this,AuthUtils.getCurrentUser(PendingCustomerActivity.this).mailAccount,response.body().responseMessage, "approval_cust_failed");

                    }
                }
                StyleableToast.makeText(PendingCustomerActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                StyleableToast.makeText(PendingCustomerActivity.this, "Sorry an error happened", R.style.Theme_GudangKita_Toast_Danger).show();
                ProgressDialogUtils.dismiss();
                t.printStackTrace();
            }
        });
    }

}