package prokoding.gudangkita.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

public class TextUtils {

    public static String simplifyAmount(int amount) {
        return simplifyAmount((float) amount);
    }

    public static String simplifyAmount(float amount) {
        float newAmount;
        int decimalPoint = 0;
        String unit;

        if (amount > 500000000) {
            newAmount = amount / 1000000000;
            unit = "B";
        } else if (amount > 500000) {
            newAmount = amount / 1000000;
            unit = "M";
        } else if (amount > 500) {
            newAmount = amount / 1000;
            unit = "K";
        } else {
            newAmount = amount;
            unit = "";
        }

        if (!unit.isEmpty()) {
            if (newAmount < 10) {
                decimalPoint = 2;
            } else if (newAmount < 100) {
                decimalPoint = 1;
            } else {
                decimalPoint = 0;
            }
        }

        return String.format("%." + decimalPoint + "f" + unit, newAmount);
//        return String.valueOf(amount);
    }

    public static BigDecimal parseCurrencyValue(String value) {
        try {
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
            numberFormat.setMaximumFractionDigits(0);
            numberFormat.setRoundingMode(RoundingMode.FLOOR);
            String replaceRegex = String.format("[%s,.\\s]", Objects.requireNonNull(numberFormat.getCurrency()).getDisplayName());
            String currencyValue = value.replaceAll(replaceRegex, "");
            return new BigDecimal(currencyValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BigDecimal.ZERO;
    }

    public static String parseCurrencyString(BigDecimal bigDecimal) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setRoundingMode(RoundingMode.FLOOR);
        try {
            return numberFormat.format(bigDecimal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return numberFormat.format(0);
    }


}
