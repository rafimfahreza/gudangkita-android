package prokoding.gudangkita.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import prokoding.gudangkita.R;
import prokoding.gudangkita.components.RemovalDialog;

public class RemovalDialogUtils {
    private static Dialog pd;
    private static final RemovalDialog.Callback callback = new RemovalDialog.Callback() {
        @Override
        public void onRemove() {
        }
    };
    private static final View.OnClickListener onCancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callback.onRemove();
            pd.dismiss();
        }
    };

    public static void show(Context context, String message) {
        if (pd == null) {
            pd = new Dialog(context);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.component_removal_dialog);
            pd.getWindow().setBackgroundDrawableResource(R.color.transparent);
            pd.getWindow().setDimAmount(0.85f);
        }
        if (message == null) {
            ((TextView) pd.findViewById(R.id.dialog_message)).setText(pd.getContext().getString(R.string.hint_loading));
        } else {
            ((TextView) pd.findViewById(R.id.dialog_message)).setText(message);
        }
        ((MaterialButton) pd.findViewById(R.id.dialog_cancel)).setOnClickListener(onCancel);
        ((FloatingActionButton) pd.findViewById(R.id.dialog_close)).setOnClickListener(onCancel);
        pd.show();
    }

    public static void dismiss() {
        pd.dismiss();
        pd.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public interface Callback {
        void onRemove();
    }
}
