package prokoding.gudangkita.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import prokoding.gudangkita.R;
import prokoding.gudangkita.api.ApiRetrofit;
import prokoding.gudangkita.models.User;
import retrofit2.Call;
import retrofit2.Response;

public class AuthUtils {
    private static FirebaseAuth firebaseAuth;
    private static GoogleSignInClient googleSignInClient;
    private static SharedPreferences sharedPreferences;

    private static FirebaseAuth getFirebaseAuth() {
        if (firebaseAuth == null) {
            firebaseAuth = FirebaseAuth.getInstance();
        }
        return firebaseAuth;
    }

    private static GoogleSignInClient getGoogleSignInClient(Context context) {
        if (googleSignInClient == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(context.getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
            googleSignInClient = GoogleSignIn.getClient(context, gso);
        }
        return googleSignInClient;
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return sharedPreferences;
    }

    public static FirebaseUser getCurrentFirebaseUser() {
        return getFirebaseAuth().getCurrentUser();
    }

    public static User.Data getCurrentUser(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        User.Data user = new User.Data();
        user.mailAccount = sharedPreferences.getString("mailAccount", null);
        user.userId = sharedPreferences.getInt("userId", -1);
        user.accountType = sharedPreferences.getInt("accountType", -1);
        user.isActive = sharedPreferences.getBoolean("isActive", false);
        user.token = sharedPreferences.getString("token", null);
        return user;
    }

    public static String getAppversion(Context context) {
        String appVersion;
        try {
            appVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            appVersion = "0";
        }
        return appVersion;
    }

    public static boolean isAuthValid(Context context) {
        User.Data user = getCurrentUser(context);
        FirebaseUser firebaseUser = getCurrentFirebaseUser();
        boolean isValid = firebaseUser != null
                && user.mailAccount != null
                && user.userId > 0
                && user.accountType > 0
                && user.token != null;
        return isValid;
    }

    public static String getWords(Context context) {
        User.Data user = getCurrentUser(context);
        String words = user.mailAccount + user.userId + user.accountType + context.getString(R.string.config_word_suffix);
        return EncryptionUtils.toHexString(EncryptionUtils.getSHA(words));
    }

    public static String getToken(Context context) {
        User.Data user = getCurrentUser(context);
        String token = user.token;
        return token;
    }

    private static void setCurrentUser(Context context, User.Data user) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferences(context).edit();
        sharedPreferencesEditor.putString("mailAccount", user.mailAccount);
        sharedPreferencesEditor.putInt("userId", user.userId);
        sharedPreferencesEditor.putInt("accountType", user.accountType);
        sharedPreferencesEditor.putBoolean("isActive", user.isActive);
        sharedPreferencesEditor.putString("token", user.token);
        sharedPreferencesEditor.commit();
    }

    public static void clearCurrentUser(Context context) {
        getFirebaseAuth().signOut();
        getGoogleSignInClient(context).signOut();
    }

    public interface Callback {
        void onAuthStart();

        void onSuccess();

        void onFailed();

        void onError();

        void onCancel();
    }

    public static class AuthLauncher {
        Context context;
        ActivityResultLauncher<Intent> authLauncher;
        Callback callback = new Callback() {

            @Override
            public void onAuthStart() {
            }

            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailed() {
            }

            @Override
            public void onError() {
            }

            @Override
            public void onCancel() {
            }
        };

        public AuthLauncher(Context context) {
            this.context = context;
            this.authLauncher = ((AppCompatActivity) context).registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>() {
                        @Override
                        public void onActivityResult(ActivityResult result) {
                            if (result.getResultCode() == Activity.RESULT_OK) {
                                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(result.getData());
                                callback.onAuthStart();
                                try {
                                    GoogleSignInAccount account = task.getResult(ApiException.class);
                                    handleFirebaseAuth(context, account.getIdToken());
                                } catch (Exception e) {
                                    getFirebaseAuth().signOut();
                                    getGoogleSignInClient(context).signOut();
                                    callback.onError();
                                }
                                return;
                            }
                            callback.onCancel();
                        }
                    }
            );
        }

        public void launch(Context context) {
            GoogleSignInClient googleSignInClient = getGoogleSignInClient(context);
            Intent intent = googleSignInClient.getSignInIntent();
            authLauncher.launch(intent);
        }

        private void handleFirebaseAuth(Context context, String idToken) {
            AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
            firebaseAuth.signInWithCredential(credential)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = firebaseAuth.getCurrentUser();
                                Call<User> call = ApiRetrofit.getApi(context).auth(user.getEmail());
                                call.enqueue(new retrofit2.Callback<User>() {
                                    @Override
                                    public void onResponse(Call<User> call, Response<User> response) {
                                        if (response.body() != null) {
                                            User.Data data = response.body().data;
                                            if (data != null) {
                                                setCurrentUser(context, data);
                                                callback.onSuccess();
                                                return;
                                            }
                                        }
                                        getFirebaseAuth().signOut();
                                        getGoogleSignInClient(context).signOut();
                                        callback.onFailed();
                                    }

                                    @Override
                                    public void onFailure(Call<User> call, Throwable t) {
                                        getFirebaseAuth().signOut();
                                        getGoogleSignInClient(context).signOut();
                                        callback.onError();
                                    }
                                });
                            } else {
                                getFirebaseAuth().signOut();
                                getGoogleSignInClient(context).signOut();
                                callback.onError();
                            }
                        }
                    });
        }

        public void setCallback(Callback callback) {
            this.callback = callback;
        }

    }
}
