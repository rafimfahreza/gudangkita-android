package prokoding.gudangkita.utils;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseHelper {

    private static FirebaseHelper shared;

    private FirebaseHelper(){}  //private constructor.

    public static FirebaseHelper getInstance(){
        if (shared == null){ //if there is no instance available... create new one
            shared = new FirebaseHelper();
        }

        return shared;
    }

    public void hitFirebaseLogEvent(Context context, String member_email, String response_code, String event_name) {
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle bundle = new Bundle();
        bundle.putString("member_email", member_email);
        bundle.putString("response_code", response_code);
        mFirebaseAnalytics.logEvent(event_name, bundle);
    }
}
