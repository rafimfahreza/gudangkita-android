package prokoding.gudangkita.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Environment;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;

public class DrawableUtils {

    public static Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    public static String writeBitmapToFile(Bitmap bitmap, String name) {
        File sd = Environment.getExternalStorageDirectory();
        String folder = "/GudangKita/Pictures/";
        String filename = name + ".jpg";
        String fullname = sd.getAbsolutePath() + folder + filename;

        try {
            File newDir = new File(sd + folder);
            newDir.mkdirs();
            File dest = new File(sd + folder, filename);
            FileOutputStream out = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return fullname;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String writeBitmapToTemp(Bitmap bitmap, String name) {
        File sd = Environment.getExternalStorageDirectory();
        String folder = "/GudangKita/.temp/";
        String filename = name + ".jpg";
        String fullname = sd.getAbsolutePath() + folder + filename;

        try {
            File newDir = new File(sd + folder);
            newDir.mkdirs();
            File dest = new File(sd + folder, filename);
            FileOutputStream out = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return fullname;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int maxSize) {
        float newWidth;
        float newHeight;
        float width = Float.valueOf(bitmap.getWidth());
        float height = Float.valueOf(bitmap.getHeight());
        if (width > height) {
            newWidth = Math.min(maxSize, width);
            newHeight = (newWidth / width) * height;
        } else {
            newHeight = Math.min(maxSize, height);
            newWidth = (newHeight / height) * width;
        }
        return Bitmap.createScaledBitmap(bitmap, Math.round(newWidth), Math.round(newHeight), true);
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, String filePath, int maxSize) {
        try {
            bitmap = resizeBitmap(bitmap, maxSize);
            ExifInterface ei = new ExifInterface(filePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return bitmap;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

}
