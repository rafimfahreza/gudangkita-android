package prokoding.gudangkita.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import prokoding.gudangkita.R;

public class ProgressDialogUtils {
    private static Dialog pd;

    public static void show(Context context, String message) {
        pd = new android.app.Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.component_progress_dialog);
        pd.getWindow().setBackgroundDrawableResource(R.color.transparent);
        pd.getWindow().setDimAmount(0.85f);
        pd.setCancelable(false);
        if (message == null) {
            ((TextView) pd.findViewById(R.id.textView6)).setText(context.getString(R.string.hint_loading));
        } else {
            ((TextView) pd.findViewById(R.id.textView6)).setText(message);
        }
        pd.show();
    }

    public static void show(Context context) {
        show(context, null);
    }

    public static void dismiss() {
        pd.dismiss();
        pd.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
