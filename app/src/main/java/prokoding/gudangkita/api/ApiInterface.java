package prokoding.gudangkita.api;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import prokoding.gudangkita.models.BaseResponse;
import prokoding.gudangkita.models.Customer;
import prokoding.gudangkita.models.CustomerHistory;
import prokoding.gudangkita.models.Inventory;
import prokoding.gudangkita.models.InventoryLogs;
import prokoding.gudangkita.models.LastSJNumber;
import prokoding.gudangkita.models.Member;
import prokoding.gudangkita.models.Notification;
import prokoding.gudangkita.models.Pending;
import prokoding.gudangkita.models.PendingCustomer;
import prokoding.gudangkita.models.PendingStockin;
import prokoding.gudangkita.models.PendingStockout;
import prokoding.gudangkita.models.Stockin;
import prokoding.gudangkita.models.StockinHistory;
import prokoding.gudangkita.models.Stockout;
import prokoding.gudangkita.models.StockoutHistory;
import prokoding.gudangkita.models.StockoutHistoryDetail;
import prokoding.gudangkita.models.User;
import prokoding.gudangkita.models.Warehouse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("auth")
    Call<User> auth(@Query("mailAccount") String mailAccount);

    @FormUrlEncoded
    @POST("pending-list")
    Call<Pending> getPendingList(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("customer")
    Call<Customer> getCustomers(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("customer/detail")
    Call<CustomerHistory> getCustomerDetails(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("custId") String custId
    );

    @FormUrlEncoded
    @POST("customer/all")
    Call<Customer> getAllCustomers(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("stocks/list-stockout")
    Call<StockoutHistory> getAllStockouts(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("stocks/list-stockin")
    Call<StockinHistory> getAllStockins(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("warehouse")
    Call<Warehouse> getWarehouses(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("products/available")
    Call<Inventory> getProducts(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("products/all")
    Call<Inventory> getAllProducts(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("products/logs")
    Call<InventoryLogs> getProductLog(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("productId") int productId
    );

    @FormUrlEncoded
    @POST("notifications")
    Call<Notification> getNotifications(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("member/list")
    Call<Member> getMembers(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("member/new")
    Call<BaseResponse> addMember(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("memberEmail") String memberEmail,
            @Field("name") String name,
            @Field("accessType") int accessType
    );

    @FormUrlEncoded
    @POST("stocks/stockin")
    Call<Stockin> addStockin(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("productId") int productId,
            @Field("productName") String productName,
            @Field("quantity") int quantity,
            @Field("warehouseId") int warehouseId,
            @Field("warehouseName") String warehouseName,
            @Field("memberId") int memberId
    );

    @Multipart
    @POST("products/new")
    Call<BaseResponse> addProduct(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("itemCode") RequestBody itemCode,
            @Part("itemName") RequestBody itemName,
            @Part("itemType") RequestBody itemType,
            @Part("baseStock") RequestBody baseStock,
            @Part MultipartBody.Part itemPicture
    );

    @Multipart
    @POST("products/update")
    Call<BaseResponse> updateProduct(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("itemId") RequestBody itemId,
            @Part("itemCode") RequestBody itemCode,
            @Part("itemName") RequestBody itemName,
            @Part("itemType") RequestBody itemType,
            @Part("baseStock") RequestBody baseStock,
            @Part MultipartBody.Part itemPicture
    );

    @Multipart
    @POST("auth")
    Call<BaseResponse> addCustomer(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("custType") RequestBody custType,
            @Part("custName") RequestBody custName,
            @Part("identityType") RequestBody identityType,
            @Part("identityNumber") RequestBody identityNumber,
            @Part("address") RequestBody address,
            @Part("province") RequestBody province,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("memberId") RequestBody memberId,
            @Part MultipartBody.Part itemPicture
    );

    @Multipart
    @POST("customer/update")
    Call<BaseResponse> updateCustomer(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("custId") RequestBody custId,
            @Part("custType") RequestBody custType,
            @Part("custName") RequestBody custName,
            @Part("identityType") RequestBody identityType,
            @Part("identityNumber") RequestBody identityNumber,
            @Part("address") RequestBody address,
            @Part("province") RequestBody province,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("memberId") RequestBody memberId,
            @Part("isApproved") RequestBody isApproved,
            @Part MultipartBody.Part identityPicture
    );

    @Multipart
    @POST("stocks/upload-sj")
    Call<BaseResponse> uploadSJ(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("stockoutId") RequestBody stockoutId,
            @Part MultipartBody.Part sjPicture
    );

    @FormUrlEncoded
    @POST("pending-list/detail")
    Call<PendingStockin> getPendingStockin(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId,
            @Field("requestType") String requestType
    );

    @FormUrlEncoded
    @POST("pending-list/detail")
    Call<PendingCustomer> getPendingCustomer(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId,
            @Field("requestType") String requestType
    );

    @FormUrlEncoded
    @POST("pending-list/detail")
    Call<PendingStockout> getPendingStockout(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId,
            @Field("requestType") String requestType
    );

    @FormUrlEncoded
    @POST("stocks/detail")
    Call<StockoutHistoryDetail> getStockoutHistoryDetails(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("stockoutId") String stockoutId
    );

    @FormUrlEncoded
    @POST("stocks/check-sj")
    Call<LastSJNumber> getLastSjNumber(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token
    );

    @Multipart
    @POST("pending-list/update-stockin")
    Call<BaseResponse> updatePendingStockin(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("refId") RequestBody refId,
            @Part("productId") RequestBody productId,
            @Part("warehouseId") RequestBody warehouseId,
            @Part("productQuantity") RequestBody productQuantity,
            @Part("poNumber") RequestBody poNumber,
            @Part("isValid") RequestBody isValid,
            @Part("memberId") RequestBody memberId,
            @Part MultipartBody.Part refPicture
    );

    @FormUrlEncoded
    @POST("pending-list/customer-approval")
    Call<BaseResponse> updatePendingCustomer(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId,
            @Field("isApproved") int isApproved,
            @Field("memberId") int memberId
    );

    @Multipart
    @POST("stocks/stockout")
    Call<Stockout> addStockout(
            @Part("appVersion") RequestBody appVersion,
            @Part("words") RequestBody words,
            @Part("token") RequestBody token,
            @Part("orderId") RequestBody orderId,
            @Part("stockoutType") RequestBody stockoutType,
            @Part("paymentType") RequestBody paymentType,
            @Part("quantity") RequestBody quantity,
            @Part("customerId") RequestBody customerId,
            @Part("customerName") RequestBody customerName,
            @Part("productId") RequestBody productId,
            @Part("productName") RequestBody productName,
            @Part("warehouseId") RequestBody warehouseId,
            @Part("warehouseName") RequestBody warehouseName,
            @Part("memberId") RequestBody memberId,
            @Part("deliveryDate") RequestBody deliveryDate,
            @Part MultipartBody.Part refPic
    );

    @FormUrlEncoded
    @POST("pending-list/stockout-cash")
    Call<BaseResponse> updatePendingStockoutCash(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId,
            @Field("productId") int productId,
            @Field("priceItem") long priceItem,
            @Field("totalAmount") long totalAmount,
            @Field("productQuantity") int productQuantity,
            @Field("orderDate") String orderDate,
            @Field("isValid") boolean isValid,
            @Field("memberId") int memberId,
            @Field("deliveryDate") String deliveryDate
    );

    @FormUrlEncoded
    @POST("pending-list/stockout-noncash")
    Call<BaseResponse> updatePendingStockoutNonCash(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId,
            @Field("stockoutNumber") int stockoutNumber,
            @Field("productId") int productId,

            @Field("noncashType") int noncashType,

            @Field("priceItem") long priceItem,
            @Field("totalAmount") long totalAmount,
            @Field("amountPaid") long amountPaid,
            @Field("amountDue") long amountDue,

            @Field("productQuantity") int productQuantity,

            @Field("orderDate") String orderDate,
            @Field("dueDate") long dueDate,

            @Field("isValid") boolean isValid,
            @Field("memberId") int memberId,
            @Field("deliveryDate") String deliveryDate
    );

    @FormUrlEncoded
    @POST("pending-list/update-status")
    Call<BaseResponse> updatePendingStockoutGeneral(
            @Field("appVersion") String appVersion,
            @Field("words") String words,
            @Field("token") String token,
            @Field("refId") String refId
    );

    @Multipart
    @POST("products/update")
    Call<BaseResponse> updateDetailProduct(@Part("appVersion") RequestBody appVersion,
                                           @Part("words") RequestBody words,
                                           @Part("token") RequestBody token,
                                           @Part("itemCode") RequestBody itemCode,
                                           @Part("itemName") RequestBody itemName,
                                           @Part("itemType") RequestBody itemType,
                                           @Part("baseStock") RequestBody baseStock,
                                           @Part MultipartBody.Part itemPicture,
                                           @Part("itemId") RequestBody itemId);
}
