package prokoding.gudangkita.api;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import prokoding.gudangkita.R;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiRetrofit {
    private static Retrofit retrofit;
    private static Retrofit retrofitRajaOngkir;
    private static ApiInterface api;
    private static ApiRajaOngkirInterface apiRajaOngkir;

    public static Retrofit getInstance(Context context) {
        if (retrofit == null) {
            String BASE_URL = context.getString(R.string.config_api_url);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(3, TimeUnit.MINUTES)
                    .connectTimeout(3, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getRajaOngkirInstance(Context context) {
        if (retrofitRajaOngkir == null) {
            String BASE_URL = context.getString(R.string.config_api_rajaongkir_url);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(3, TimeUnit.MINUTES)
                    .connectTimeout(3, TimeUnit.MINUTES)
                    .build();
            retrofitRajaOngkir = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofitRajaOngkir;
    }

    public static ApiInterface getApi(Context context) {
        if (api == null) {
            api = getInstance(context).create(ApiInterface.class);
        }
        return api;
    }

    public static ApiRajaOngkirInterface getRajaOngkirApi(Context context) {
        if (apiRajaOngkir == null) {
            apiRajaOngkir = getRajaOngkirInstance(context).create(ApiRajaOngkirInterface.class);
        }
        return apiRajaOngkir;
    }

}
