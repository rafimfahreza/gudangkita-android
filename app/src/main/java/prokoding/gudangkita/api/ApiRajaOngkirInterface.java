package prokoding.gudangkita.api;

import prokoding.gudangkita.models.RajaOngkirProvince;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiRajaOngkirInterface {
    @GET("province")
    Call<RajaOngkirProvince> getProvinces(@Query("key") String key);
}
